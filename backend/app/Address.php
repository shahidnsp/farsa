<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['name', 'phone', 'pincode', 'street', 'landmark','phone2','address','users_id','email'];


}
