<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTable extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'datetime', 'attendances','notified'];


}
