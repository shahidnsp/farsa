<?php

namespace App\Helper;

class SMSHelper
{
    public static function sendSMS($phone,$message)
    {
        $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?country=91&sender=FARSAR&route=4&mobiles=".$phone."&authkey=252327ADlbfutNNIF5c187c37&encrypt=&message=".$message,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
               return "cURL Error #:" . $err;
            } else {
                return $response;
            }
    }
}

?>