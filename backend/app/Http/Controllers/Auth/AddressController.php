<?php

namespace App\Http\Controllers\Auth;

use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;

class AddressController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'phone' => 'required',
            'street' => 'required',
            'landmark' => 'required',
        ]);
    }

    public function submitMyDeliveryAddress(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $address = new Address($request->all());
        $address->users_id=Auth::id();
        if ($address->save()) {
            return redirect('/account');
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Address::all();
    }
}
