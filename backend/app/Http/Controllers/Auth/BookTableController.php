<?php

namespace App\Http\Controllers\Auth;

use App\BookTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;
class BookTableController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'datetime' => 'required',
            'attendances' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return BookTable::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = new BookTable($request->all());
        if ($offer->save()) {
            return $offer;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    public function bookMyTable(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = new BookTable($request->all());
        if ($offer->save()) {
            return 'success';
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $booktable = BookTable::find($id);
        $booktable->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $booktable->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($booktable->update()) {
            return $booktable;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (BookTable::destroy($id)) {
            return Response::json(array('msg' => 'BookTable record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeBookTableStatus(Request $request){
        $id=$request->id;
        $booktable=BookTable::findOrfail($id);
        if($booktable){
            if($booktable->notified==1)
                $booktable->notified=0;
            else
                $booktable->notified=1;
            if($booktable->save())
                return $booktable;
        }
    }
}
