<?php

namespace App\Http\Controllers\Auth;

use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Validator;
use Response;
use Image;

class MenuController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Menu::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $menu = new Menu($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $menu->photo = $this->savePhoto($photo['data']);
            }
        }else{
            $menu->photo ='defaultmenu.jpg';
        }
        if ($menu->save()) {
            Cache::flush();
            return $menu;
        }

        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'menu'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(620, 542)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $menu = Menu::find($id);
        $menu->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $menu->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($menu->update()) {
            Cache::flush();
            return $menu;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Menu::find($id);
        if($items){
            if ($items->photo != 'defaultmenu.jpg') {
                $exist = Storage::disk('local')->exists($items->photo);
                if ($exist)
                    Storage::delete($items->photo);
            }
        }
        if (Menu::destroy($id)) {
            Cache::flush();
            return Response::json(array('msg' => 'Menu record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeMenuStatus(Request $request){
        $id=$request->id;
        $menu=Menu::findOrfail($id);
        if($menu){
            if($menu->active==1)
                $menu->active=0;
            else
                $menu->active=1;

            Cache::flush();
            if($menu->save())
                return $menu;
        }
    }
}
