<?php

namespace App\Http\Controllers\Auth;

use App\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use Image;

class OfferController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'products_id' => 'required',
            'amount' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Offer::with('product')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = new Offer($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $offer->photo = $this->savePhoto($photo['data']);
            }
        }else{
            $offer->photo ='offer.jpg';
        }
        if ($offer->save()) {
            return Offer::with('product')->ffind($offer->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }


    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'offer'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(600, 420)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = Offer::find($id);
        $offer->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $offer->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($offer->update()) {
            return Offer::with('product')->ffind($offer->id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Offer::find($id);
        if($items){
            if ($items->photo != 'offer.jpg') {
                $exist = Storage::disk('local')->exists($items->photo);
                if ($exist)
                    Storage::delete($items->photo);
            }
        }
        if (Offer::destroy($id)) {
            return Response::json(array('msg' => 'Offer record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

}
