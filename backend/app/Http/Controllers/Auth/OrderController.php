<?php

namespace App\Http\Controllers\Auth;

use App\Address;
use App\Helper\GST;
use App\Order;
use App\OrderItem;
use App\ProductPrice;
use App\WeekendOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Exception;
use Validator;
use Response;
use Indipay;

class OrderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'datetime' => 'required',
            'addresses_id' => 'required',
            'weekend_offers_id' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;

        if($fromDate==null || $toDate==null) {
            return Order::with('user')->with('weekendoffer')->with('address')->with('orderitems.product.product')->get();
        }
        else{
            $paymentstatus=$request->paymentstatus;
            $status=$request->status;
            if($status==true){
                return Order::where('paymentstatus', $paymentstatus)->with('user')->with('weekendoffer')->with('address')->with('orderitems.product.product')->get();
            }else {
                if ($paymentstatus == null) {
                    $from = Carbon::parse($fromDate)->startOfDay();
                    $to = Carbon::parse($toDate)->endOfDay();
                    return Order::where('datetime', '>=', $from)->where('datetime', '<=', $to)->with('user')->with('weekendoffer')->with('address')->with('orderitems.product.product')->get();
                } else {
                    $from = Carbon::parse($fromDate)->startOfDay();
                    $to = Carbon::parse($toDate)->endOfDay();
                    return Order::where('datetime', '>=', $from)->where('paymentstatus', $paymentstatus)->where('datetime', '<=', $to)->with('user')->with('weekendoffer')->with('address')->with('orderitems.product.product')->get();
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $order = new Order($request->all());
        if ($order->save()) {
            return $order;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

   
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $order = Order::find($id);
        $order->fill($request->all());
        if ($order->update()) {
            return $order;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Order::destroy($id)) {
            return Response::json(array('msg' => 'Order record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

   /* public function changeOrderStatus(Request $request){
        $id=$request->id;
        $order=Order::findOrfail($id);
        if($order){
            if($order->active==1)
                $order->active=0;
            else
                $order->active=1;
            if($order->save())
                return $order;
        }
    }*/


    public function takeMyOrder(Request $request){

        $price=ProductPrice::findOrfail($request->products_id);

        if($price) {

            $day=Carbon::now()->format('l');
            $discountper=0;
            $offerper=WeekendOffer::where('day',$day)->where('active',1)->get()->first();
            if($offerper!=null){
                $discountper=$offerper->amount;
            }

            $order = Order::where('users_id', Auth::id())->where('paymentstatus', 0)->get()->last();
            if ($order == null) {

                $address_id = 0;

                $address = Address::where('users_id', Auth::id())->get()->last();
                if ($address != null)
                    $address_id = $address->id;

                $order = new Order();
                $order->orderno = $this->generateOrderNo();
                $order->datetime = Carbon::now();
                $order->users_id = Auth::id();
                $order->addresses_id = $address_id;
                $order->total = 0;
                $order->pretotal = 0;
                $order->discount = $discountper;
                $order->gst = 0;
                $order->deliverycharge = 0;
                $order->grandtotal = 0;
                $order->paymentstatus = 0;
                $order->save();
            }

            $preOrder=OrderItem::where('orders_id',$order->id)->where('products_id',$request->products_id)->get()->first();
            if($preOrder!=null){
                $preamount=$preOrder->amount;
                $preqty=$preOrder->qty;
                $preTotal=$preamount*$preqty;

                $preOrderAmount=$order->total;
                $curOrderAmount=$preOrderAmount-$preTotal;

                $order->total=$curOrderAmount;
                $order->save();

                OrderItem::where('orders_id',$order->id)->where('products_id',$request->products_id)->delete();
            }

            $order_item = new OrderItem();
            $order_item->orders_id = $order->id;
            $order_item->products_id = $request->products_id;
            $order_item->qty = $request->qty;
            $order_item->amount = $price->amount;
            if($order_item->save()){
                $amount=$price->amount;
                $qty= $request->qty;
                $subItemTotal=$qty*$amount;
                $subtotal=$order->total;
                $curTotal=$subtotal+$subItemTotal;
                $gstrate=GST::$gstamount; //GST AMOUNT SAVED IN Helper/GST.php
                $gstamount=($curTotal*$gstrate)/100;

                $discount=($curTotal*$order->discount)/100;
                $order->total=$curTotal;
                $order->pretotal=$curTotal-$discount;
                $order->gst=$gstamount;
                $order->grandtotal=$order->pretotal+$gstamount;
                if($order->save())
                     return redirect('/menu');
            }else{
                return redirect('/menu');
            }
        }
    }

    private function generateOrderNo(){
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        return $today.$rand;
    }

    public function removeMyOrder(Request $request){
        $products_id=$request->products_id;
        $orders_id=$request->orders_id;

        $order = Order::findOrfail($orders_id);
        if ($order) {
            $preOrder=OrderItem::where('orders_id',$order->id)->where('products_id',$products_id)->get()->first();
            if($preOrder!=null){
                $preamount=$preOrder->amount;
                $preqty=$preOrder->qty;
                $preTotal=$preamount*$preqty;

                $preOrderAmount=$order->total;
                $curOrderAmount=$preOrderAmount-$preTotal;
                $discount=($curOrderAmount*$order->discount)/100;
                $order->pretotal=$curOrderAmount-$discount;

                $gstrate=GST::$gstamount; //GST AMOUNT SAVED IN Helper/GST.php
                $gstamount=($order->pretotal*$gstrate)/100;

                $order->total=$curOrderAmount;

                $order->gst=$gstamount;
                $order->grandtotal=$order->pretotal+$gstamount;
                if($order->save()){
                    OrderItem::where('orders_id',$order->id)->where('products_id',$products_id)->delete();
                    return redirect('/menu');
                }

            }
        }
        return redirect('/menu');
    }

    public function makeMyPayment(Request $request){
        $orders_id=$request->orders_id;

        //Address
        $name=$request->name;
        $phone=$request->phone;
        $pincode=$request->pincode;
        $street=$request->street;
        $landmark=$request->landmark;
        $phone2=$request->phone2;
        $address=$request->address;

        $deliverytime=$request->deliverytime;
        $paymenttype=$request->paymenttype;

        $curAddress=new Address();
        $curAddress->name=$name;
        $curAddress->phone=$phone;
        $curAddress->pincode=$pincode;
        $curAddress->street=$street;
        $curAddress->landmark=$landmark;
        $curAddress->phone2=$phone2;
        $curAddress->address=$address;
        $curAddress->users_id=Auth::id();

        if($curAddress->save()){
            if($orders_id!=null) {
                $order = Order::findOrfail($orders_id);
                if ($order) {
                    if (count($order->orderitems) == 0) {
                        return redirect('/menu');
                    }
                    $order->deliverytime = $deliverytime;
                    $order->paymenttype = $paymenttype;
                    $order->addresses_id = $curAddress->id;
                    if ($order->save()) {
                        if ($paymenttype == 'COD') {
                            $order->paymentstatus = 1;
                            if ($order->save()) {
                                try{
                                    $this->sendThankSMS($name,$phone,$order->grandtotal,'COD');
                                }catch (Exception $e){}
                                return redirect('/payment_success');
                            }
                        } else {
                            $tid = time() . rand(111, 999);
                            $parameters = [
                                'tid' => $tid,
                                'order_id' => $order->orderno,
                                'firstname' => Auth::user()->name,
                                'email' => Auth::user()->email,
                                'phone' => $phone,
                                'productinfo' => "prodyyyyy",
                                'amount' => $order->grandtotal,
                                'udf1' => $tid,
                                'udf2' => $paymenttype,
                                'udf3' => Auth::id(),
                                'udf4' => $order->id,
                                'udf5' => $order->orderno
                            ];

                            $order = Indipay::prepare($parameters);
                            return Indipay::process($order);
                        }
                    }
                }
            }
        }

        return redirect('/menu');
    }

    private function sendThankSMS($name,$phone,$amount,$type){
        if($phone!=null) {
            if ($type == 'COD') {
                \App\Helper\SMSHelper::sendSMS($phone, 'Dear ' . $name . ', Thank You for using our Service.Pay  ' . $amount . ' on Delivery');

                \App\Helper\SMSHelper::sendSMS('8136806368', 'New Order: '.$amount.' by COD. Customer: '.$name.' ,Phone: '.$phone.' Arrange Delivery');
            }
            else {
                \App\Helper\SMSHelper::sendSMS($phone, 'Dear ' . $name . ', Thank You for using our Service.');

                \App\Helper\SMSHelper::sendSMS('8136806368', 'New Order: '.$amount.' by Online. Customer: '.$name.' ,Phone: '.$phone.' .Arrange Delivery');
            }

        }
    }

    public function successResponse(Request $request)

    {
        // For default Gateway
        $response = Indipay::response($request);

        $order_id=$response['udf4'];
        $status=$response['status'];
        $reason=$response['field9'];
        //$unmappedReason=$response['unmappedstatus'];
        $order=Order::findOrfail($order_id);
        if($order){
            $order->paymentstatus=1;
            $order->status=$status;
            $order->failurereason=$reason;
            if($order->save()){
                try{
                    $address=Address::find($order->addresses_id);
                    if($address!=null)
                        $this->sendThankSMS($address->name,$address->phone,$order->grandtotal,'Online');
                }catch (Exception $e){}
                return redirect('/payment_success');
            }
        }

        return redirect('/checkout');

    }

    public function failureResponse(Request $request){
        // For default Gateway
        $response = Indipay::response($request);

        $order_id=$response['udf4'];
        $status=$response['status'];
        $reason=$response['field9'];
        $unmappedReason=$response['unmappedstatus'];

        if($unmappedReason!='userCancelled'){
            $order=Order::findOrfail($order_id);
            if($order){
                $order->paymentstatus=2;
                $order->status=$status;
                $order->failurereason=$reason;
                if($order->save()){
                    return redirect('/payment_failed');
                }
            }
        }
        return redirect('/checkout');
    }

    public function getOrderPdf(Request $request){

        $from=$request->fromDate;
        $to=$request->toDate;

        $fromDate=Carbon::parse($from)->startOfDay();
        $toDate=Carbon::parse($to)->endOfDay();

        $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');

        $orders=\App\Order::with('orderitems.product.product')->where('deliverystatus','Pending')->with('address')->where('paymentstatus',1)->where('datetime','>=',$fromDate)->where('datetime','<=',$toDate)->get();

        $data='<!DOCTYPE html><html><head><style>table, td, th,tr {border: 0.01em solid #858b8c;} td{padding-left: 10px;padding-right: 5px;padding-bottom: 5px;padding-top: 5px;}</style></head><body>';
        $data=$data.'<h2 style="text-align: center;">Order Details</h2>';
        $data=$data.'<table  style="width:100%;"><tr><th>SlNo</th><th>Order/Date</th><th>Name & Address</th><th>Orders</th><th>Payment Type</th><th>Amount</th></tr>';

        foreach($orders as $index=> $order) {
            $data = $data . '<tr><td>'.($index+1).'</td><td><span style="color:blue;">'.$order->orderno.'</span>-'.\Illuminate\Support\Carbon::parse($order->datetime)->format('d/m/y').'</td>';

            $data=$data.'<td>'.$order->address->name.'<br/>'.$order->address->phone.','.$order->address->phone2.'<br/>'.$order->address->street.'<br/>'.$order->address->landmark.'<br/>'.$order->address->address.'</td>';

            $data = $data . '<td>';
            $data = $data . '<table><tr><th>Item</th><th>Qty</th><th>E.Price</th></tr>';
            foreach($order->orderitems as $item) {
                if($item->product!=null)
                    $data = $data . '<tr><td>'.$item->product->product->name.'<br/>'.$item->product->name.'</td><td>'.$item->qty.'</td><td>'.$item->product->amount.'</td></tr>';
            }
            $data = $data . '<tr style="color:blue;"><td colspan="2">GST</td><td>'.$order->gst.'</td></tr>';
            $data = $data . '<tr style="color:blue;"><td colspan="2">Delivery</td><td>'.$order->deliverycharge.'</td></tr>';
            $data = $data . '</table>';
            $data = $data . '</td>';

            $color='';
            if($order->paymenttype=="COD")
                $color='red';
            $data=$data.'<td style="color:'.$color.'">'.$order->paymenttype.'</td><td style="color:blue;">'.$order->grandtotal.'</td></tr>';
        }
        $data=$data.'</table></body></html>';



        $pdf->loadHTML($data);
        $pdf->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function changeOrderStatus(Request $request){
        $id=$request->id;
        $order=Order::findOrfail($id);
        if($order){
            if($order->deliverystatus=='Pending') {
                $order->deliverystatus = 'Placed';
                $order->deliverydate = Carbon::now();
            }
            else
                $order->deliverystatus='Pending';
            if($order->save())
                return $order;
        }
    }
}
