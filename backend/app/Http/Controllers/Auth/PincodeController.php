<?php

namespace App\Http\Controllers\Auth;

use App\Pincode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class PincodeController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'pincode' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Pincode::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $pincode = new Pincode($request->all());

        if ($pincode->save()) {
            return $pincode;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $pincode = Pincode::find($id);
        $pincode->fill($request->all());

        if ($pincode->update()) {
            return $pincode;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Pincode::destroy($id)) {
            return Response::json(array('msg' => 'Pincode record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changePincodeStatus(Request $request){
        $id=$request->id;
        $pincode=Pincode::findOrfail($id);
        if($pincode){
            if($pincode->active==1)
                $pincode->active=0;
            else
                $pincode->active=1;
            if($pincode->save())
                return $pincode;
        }
    }
}
