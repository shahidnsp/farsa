<?php

namespace App\Http\Controllers\Auth;

use App\Product;
use App\ProductPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Validator;
use Response;
use Image;

class ProductController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'menues_id' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Product::with('menu')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $product = new Product($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $product->photo = $this->savePhoto($photo['data']);
            }
        }else{
            $product->photo ='defaultproduct.jpg';
        }
        if ($product->save()) {

            $prices=$request->prices;
            foreach($prices as $price){
                $product_price=new ProductPrice();
                $product_price->name=$price['name'];
                $product_price->amount=$price['amount'];
                $product_price->products_id=$product->id;
                $product_price->save();
            }
            Cache::flush();

            return $product;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'testimonial'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(1200, 600)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $product = Product::find($id);
        $product->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $product->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($product->update()) {

            ProductPrice::where('products_id',$product->id)->delete();

            $prices=$request->prices;
            foreach($prices as $price){
                $product_price=new ProductPrice();
                $product_price->name=$price['name'];
                $product_price->amount=$price['amount'];
                $product_price->products_id=$product->id;
                $product_price->save();
            }
            Cache::flush();
            return $product;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Product::find($id);
        if($items){
            if ($items->photo != 'defaultproduct.jpg') {
                $exist = Storage::disk('local')->exists($items->photo);
                if ($exist)
                    Storage::delete($items->photo);
            }
        }
        if (Product::destroy($id)) {
            ProductPrice::where('products_id',$id)->delete();
            Cache::flush();
            return Response::json(array('msg' => 'Testimonial record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeProductStatus(Request $request){
        $id=$request->id;
        $product=Product::findOrfail($id);
        if($product){
            if($product->active==1)
                $product->active=0;
            else
                $product->active=1;
            Cache::flush();
            if($product->save())
                return $product;
        }
    }

    public function getProductPriceList(Request $request){
        $id=$request->id;

        return ProductPrice::where('products_id',$id)->get();
    }
}
