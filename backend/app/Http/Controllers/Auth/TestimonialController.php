<?php

namespace App\Http\Controllers\Auth;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use Image;

class TestimonialController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Testimonial::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $testimonial = new Testimonial($request->all());
        $testimonial->rating=5;
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $testimonial->photo = $this->savePhoto($photo['data']);
            }
        }else{
            $testimonial->photo ='profile.png';
        }
        if ($testimonial->save()) {
            return $testimonial;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    public function submitMyReview(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $testimonial = new Testimonial($request->all());
        $testimonial->rating=5;
        $testimonial->active=0;
        $testimonial->photo ='profile.png';
        if ($testimonial->save()) {
            return redirect('/reviews');
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'testimonial'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(256, 256)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $testimonial = Testimonial::find($id);
        $testimonial->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $testimonial->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($testimonial->update()) {
            return $testimonial;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Testimonial::find($id);
        if($items){
            if ($items->photo != 'profile.png') {
                $exist = Storage::disk('local')->exists($items->photo);
                if ($exist)
                    Storage::delete($items->photo);
            }
        }
        if (Testimonial::destroy($id)) {
            return Response::json(array('msg' => 'Testimonial record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeTestimonialStatus(Request $request){
        $id=$request->id;
        $user=Testimonial::findOrfail($id);
        if($user){
            if($user->active==1)
                $user->active=0;
            else
                $user->active=1;
            if($user->save())
                return $user;
        }
    }
}
