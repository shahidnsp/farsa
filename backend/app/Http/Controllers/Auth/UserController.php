<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use Image;

class UserController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'name' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usertype=$request->usertype;
        return User::where('usertype',$usertype)->where('id','!=',Auth::id())->get();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $user = new User($request->all());
        $user->password=bcrypt('admin123');
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $user->photo = $this->savePhoto($photo['data']);
            }
        }else{
            $user->photo ='profile.png';
        }
        if ($user->save()) {
            return $user;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'profile'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(256, 256)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $user = User::find($id);
        $user->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $user->photo = $this->savePhoto($photo['data']);
            }
        }
        if ($user->update()) {
            return $user;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=User::find($id);
        if($items){
            if ($items->photo != 'profile.png') {
                $exist = Storage::disk('local')->exists($items->photo);
                if ($exist)
                    Storage::delete($items->photo);
            }
        }
        if (User::destroy($id)) {
            return Response::json(array('msg' => 'User record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeUserStatus(Request $request){
        $id=$request->id;
        $user=User::findOrfail($id);
        if($user){
            if($user->active==1)
                $user->active=0;
            else
                $user->active=1;
            if($user->save())
                return $user;
        }
    }

    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function ChangePasswordValidator(array $data)
    {
        return Validator::make($data, [
            'oldpassword' => 'required',
            'newpassword' => 'required',
            'confirmpassword' => 'required',
        ]);
    }
    public function changeMyPassword(Request $request){
        $validator = $this->ChangePasswordValidator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        try{

            $user = User::findOrFail(Auth::id());
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'Requested user not found'],404);
        }

        if($user->changePassword($request->oldpassword,$request->newpassword)){
            if($user->save()){
                Auth::logout();
                return redirect('/home');
            }
        }
        else
            return redirect('/account');

    }

    public function changeAdminPassword(Request $request){
        $validator = $this->ChangePasswordValidator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        try{

            $user = User::findOrFail(Auth::id());
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'Requested user not found'],404);
        }

        if($user->changePassword($request->oldpassword,$request->newpassword)){
            if($user->save()){
               return $user;
            }
        }
        return Response::json(['error'=>'Invalid Request'],404);
    }

    public function updateMyProfile(Request $request){
        $name=$request->name;
        $email=$request->email;
        $photos=$request->photos;

        $user=User::find(Auth::id());
        if($user!=null){
            if($name!=null){
                $user->name=$name;
            }
            if($email!=null){
                $user->email=$email;
            }

            if($photos!=null){
                foreach ($photos as $photo) {
                    $user->photo = $this->savePhoto($photo['data']);
                }
            }

            if($user->save()){
                return $user;
            }
        }
        return Response::json(array('error' => 'Record Not found'), 400);
    }
}
