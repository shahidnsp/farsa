<?php

namespace App\Http\Controllers\Auth;

use App\WeekendOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class WeekendOfferController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'day' => 'required',
            'amount' => 'required',
            'gratherthanamount' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return WeekendOffer::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = new WeekendOffer($request->all());
        if ($offer->save()) {
            return $offer;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $offer = WeekendOffer::find($id);
        $offer->fill($request->all());
        if ($offer->update()) {
            return $offer;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (WeekendOffer::destroy($id)) {
            return Response::json(array('msg' => 'Weekend Offer record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeWeekendOfferStatus(Request $request){
        $id=$request->id;
        $offer=WeekendOffer::findOrfail($id);
        if($offer){
            if($offer->active==1)
                $offer->active=0;
            else
                $offer->active=1;
            if($offer->save())
                return $offer;
        }
    }
}
