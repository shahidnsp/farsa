<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->usertype=='Admin')
             return view('app.index');
        else
            return view('frontend.index');
    }

    public function getMyProfile(){
        return User::find(Auth::id());
    }


    public function getDashboardInfo(){
        $noofusers=User::where('usertype','User')->count();
        $nooforders=Order::where('paymentstatus',1)->where('deliverystatus','Pending')->count();
        $totalorders=Order::where('paymentstatus',1)->count();
        $totalproducts=Product::where('active',1)->count();

        $lists['noofusers']=$noofusers;
        $lists['nooforders']=$nooforders;
        $lists['totalorders']=$totalorders;
        $lists['totalproducts']=$totalproducts;

        $neworders=[];
        $orders=Order::where('paymentstatus',1)->with('address')->where('deliverystatus','Pending')->get();
        foreach($orders as $order){
            $neworder['id']=$order->id;
            $neworder['customer']=$order->address->name;
            $neworder['amount']=$order->grandtotal;
            $neworder['time']=Carbon::parse($order->created_at)->diffForHumans();
            $neworder['order']=$order;
            array_push($neworders,$neworder);
        }

        $lists['pending_orders']=$neworders;

        $placedorders=[];
        $orders=Order::where('paymentstatus',1)->with('address')->where('deliverystatus','Placed')->where('deliverydate','>=',Carbon::now()->startOfDay())->where('deliverydate','<=',Carbon::now()->endOfDay())->get();
        foreach($orders as $order){
            $neworder['id']=$order->id;
            $neworder['customer']=$order->address->name;
            $neworder['amount']=$order->grandtotal;
            $neworder['time']=Carbon::parse($order->created_at)->diffForHumans();
            $neworder['order']=$order;
            array_push($placedorders,$neworder);
        }

        $lists['placed_orders']=$placedorders;

        return $lists;
    }
}
