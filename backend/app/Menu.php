<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table='menues';
    protected $fillable = ['name',  'photo', 'description', 'active'];

    public function products()
    {
        return $this->hasMany('App\Product','menues_id');
    }
}
