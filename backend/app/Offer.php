<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['name','amount', 'description',  'products_id','photo'];

    public function product()
    {
        return $this->belongsTo('App\Product','products_id');
    }
}
