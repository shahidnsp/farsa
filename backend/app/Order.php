<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name','datetime','orderno', 'users_id','addresses_id','total','gst','deliverycharge','grandtotal','paymentstatus','weekend_offers_id','paymenttype'];

    public function user(){
        return $this->belongsTo('App\User','users_id');
    }

    public function address(){
        return $this->belongsTo('App\Address','addresses_id');
    }

    public function weekendoffer(){
        return $this->belongsTo('App\WeekendOffer','weekend_offers_id');
    }

    public function orderitems(){
        return $this->hasMany('App\OrderItem','orders_id');
    }
}
