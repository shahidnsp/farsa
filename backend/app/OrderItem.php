<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [ 'amount', 'qty', 'orders_id','products_id'];

    public function product()
    {
        return $this->belongsTo('App\ProductPrice','products_id');
    }
}
