<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'photo', 'menues_id', 'active'];

    public function menu()
    {
        return $this->belongsTo('App\Menu','menues_id');
    }

    public function prices()
    {
        return $this->hasMany('App\ProductPrice','products_id');
    }
}
