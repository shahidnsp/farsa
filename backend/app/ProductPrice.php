<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $fillable = ['name', 'amount', 'products_id'];

    public function product()
    {
        return $this->belongsTo('App\Product','products_id');
    }
}
