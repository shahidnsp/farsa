<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['name', 'amount', 'photo',  'products_id'];

    public function product()
    {
        return $this->belongsTo('App\Product','products_id');
    }
}
