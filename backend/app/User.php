<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','usertype','photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] =bcrypt($password);
    }*/

    /**
     * Change password or current password
     * @param $oldPassword
     * @param $password
     * @return boolean true if password changed and false if old password doesn't match
     */
    public function changePassword($oldPassword, $password)
    {
        if(Hash::check($oldPassword,$this->attributes['password'])){
            $this->attributes['password'] = bcrypt($password);
            return true;
        }
        return false;
    }

    public function getIsAdminAttribute()
    {
        return true;
    }
}
