<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'photo' => 'profile.png',
        'usertype' => $faker->randomElement(['Admin','User']),
        'active' => $faker->randomElement([1,0]),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Menu::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'photo' => 'defaultmenu.jpg',
        'description' => $faker->sentence,
        'active' => $faker->randomElement([1,0]),
    ];
});


$factory->define(App\WeekendOffer::class, function (Faker $faker) {


    return [
        'day' => $faker->randomElement(['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']),
        'amount' =>  $faker->randomElement([1222,1340]),
        'gratherthanamount' =>  $faker->randomElement([1222,1340]),
        'active' => $faker->randomElement([1,0]),
    ];
});

$factory->define(App\Address::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'phone' =>  $faker->phoneNumber,
        'pincode' => $faker->postcode,
        'street' => $faker->sentence(12),
        'landmark' => $faker->sentence(12),
        'phone2' => $faker->phoneNumber,
        'address' => $faker->sentence,
        'email' => $faker->email,
        'users_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Order::class, function (Faker $faker) {


    return [
        'orderno' => $faker->numberBetween(100,1000),
        'datetime' =>  $faker->date,
        'users_id' => $faker->randomElement([1,2,3,4,5]),
        'addresses_id' => $faker->randomElement([1,2,3,4,5]),
        'total' => $faker->randomElement([891,657]),
        'gst' => $faker->randomElement([1,5]),
        'deliverycharge' => $faker->randomElement([891,657]),
        'grandtotal' => $faker->randomElement([891,657]),
        'paymentstatus' => $faker->randomElement([1,0,2]),
        'paymenttype' => $faker->randomElement(['Online','COD']),
        'weekend_offers_id' =>$faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Slider::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'amount' =>  $faker->numberBetween(100,1000),
        'photo' => 'slider.jpg',
        'products_id' =>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Pincode Seed faker
$factory->define(App\Pincode::class,function($faker){
    return [
        'name'=>$faker->name,
        'pincode'=>$faker->postcode,
        'active'=>$faker->randomElement([1,0]),
    ];
});

$factory->define(App\Offer::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'photo' => 'offer.jpg',
        'amount' =>  $faker->numberBetween(100,1000),
        'description' => $faker->sentence,
        'products_id' =>$faker->randomElement([1,2,3,4,5]),

    ];
});
$factory->define(App\OrderItem::class, function (Faker $faker) {


    return [
        'amount' =>  $faker->numberBetween(100,1000),
        'qty' =>  $faker->numberBetween(1,10),
        'orders_id' =>$faker->randomElement([1,2,3,4,5]),
        'products_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Testimonial::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'designation' =>  $faker->sentence,
        'description' => $faker->sentence,
        'photo' => 'profile.png',
        'rating' => $faker->randomElement([1,5]),
        'active' => $faker->randomElement([1,0]),
    ];
});
$factory->define(App\Product::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'photo' => 'defaultproduct.jpg',
        'menues_id' => $faker->randomElement([1,2,3,4,5]),
        'active' => $faker->randomElement([1,0]),

    ];
});
$factory->define(App\BookTable::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'email'=> $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'datetime' => $faker->date,
        'attendances' => $faker->randomElement([81,7]),
        'notified' => $faker->randomElement([1,0]),
    ];
});
$factory->define(App\ProductPrice::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'amount' =>  $faker->numberBetween(100,1000),
        'products_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});
