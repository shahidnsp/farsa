<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderno')->default('001');
            $table->dateTime('datetime');
            $table->integer('users_id');
            $table->integer('addresses_id');
            $table->decimal('total',8,2);
            $table->decimal('discount',8,2)->default(0);
            $table->decimal('pretotal',8,2)->default(0);
            $table->decimal('gst',8,2);
            $table->decimal('deliverycharge',8,2);
            $table->decimal('grandtotal',8,2);
            $table->integer('paymentstatus')->default(0);
            $table->integer('weekend_offers_id')->nullable();
            $table->string('status')->nullable();
            $table->string('failurereason')->nullable();
            $table->string('deliverytime')->nullable();
            $table->dateTime('deliverydate')->nullable();
            $table->string('paymenttype')->nullable();
            $table->string('deliverystatus')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
