<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();

        \App\User::create([
            'email'=>'admin@admin.com',
            'password'=>bcrypt('admin'),
            'name'=>'Admin',
            'usertype'=>'Admin',
            'photo'=>'profile.png']);

        \App\User::create([
            'email'=>'user@admin.com',
            'password'=>bcrypt('admin'),
            'name'=>'User',
            'usertype'=>'User',
            'photo'=>'profile.png']);

        $info->comment('Admin user created');
        $info->error('Username : admin@admin.com Password:admin');

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Menu table seeding started...');
        $this->call('MenuSeeder');

        $info->info('WeedendOffer table seeding started...');
        $this->call('WeekendOfferSeeder');

        $info->info('Addresss table seeding started...');
        $this->call('AddressSeeder');

        $info->info('Odrer table seeding started...');
        $this->call('OrderSeeder');

        $info->info('Slider table seeding started...');
        $this->call('SliderSeeder');

        $info->info('Offer table seeding started...');
        $this->call('OfferSeeder');

        $info->info('OrderItem table seeding started...');
        $this->call('OrderItemSeeder');

        $info->info('Testimonial table seeding started...');
        $this->call('TestimonialSeeder');

        $info->info('Product table seeding started...');
        $this->call('ProductSeeder');

        $info->info('BookTable table seeding started...');
        $this->call('BookTableSeeder');

        $info->info('ProuctPrice table seeding started...');
        $this->call('ProductPriceSeeder');

        $info->info('Pincode table seeding started...');
        $this->call('PincodeSeeder');


        $info->error('Seeding Completed.........');
        Model::reguard();

    }
}
