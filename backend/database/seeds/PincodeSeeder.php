<?php

use Illuminate\Database\Seeder;

class PincodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pincode=factory(App\Pincode::class,10)->create();
    }
}
