

var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'ngNotify',
        'cp.ngConfirm',
        'textAngular',
        'UserService',
        'TestimonialService',
        'MenuService',
        'ProductService',
        'WeekendOfferService',
        'OfferService',
        'BookedTableService',
        'SliderService',
        'OrderService',
        'AddressService',
        'PincodeService',
    ]);

/*app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});*/
app.config(function($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'template/dashboard',
            controller: 'DashboardController'
        })
        .when('/dashboard', {
            templateUrl: 'template/dashboard',
            controller: 'DashboardController'
        })
        .when('/clients', {
            templateUrl: 'template/clients',
            controller: 'ClientController'
        })
        .when('/administrator', {
            templateUrl: 'template/administrator',
            controller: 'AdministratorController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/menu', {
            templateUrl: 'template/menu',
            controller: 'MenuController'
        })
        .when('/product', {
            templateUrl: 'template/product',
            controller: 'ProductController'
        })
        .when('/weekendoffer', {
            templateUrl: 'template/weekendoffer',
            controller: 'WeekendOfferController'
        })
        .when('/offers', {
            templateUrl: 'template/offers',
            controller: 'OfferController'
        })
        .when('/slider', {
            templateUrl: 'template/slider',
            controller: 'SliderController'
        })
        .when('/bookedtables', {
            templateUrl: 'template/bookedtables',
            controller: 'BookedTableController'
        })
        .when('/order', {
            templateUrl: 'template/order',
            controller: 'OrderController'
        })
        .when('/ordermanagement', {
            templateUrl: 'template/ordermanagement',
            controller: 'OrderManagementController'
        })
        .when('/pincode', {
            templateUrl: 'template/pincode',
            controller: 'PincodeController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'ProfileController'
        })
        .when('/changepassword', {
            templateUrl: 'template/changepassword',
            controller: 'ChangePasswordController'
        })
        .otherwise({
            redirectTo: 'template/dashboard'
        });
});

app.directive('showDuringResolve', function($rootScope) {

    return {
        link: function(scope, element) {

            element.addClass('ng-hide');

            var unregister = $rootScope.$on('$routeChangeStart', function() {
                element.removeClass('ng-hide');
            });

            scope.$on('$destroy', unregister);
        }
    };
});

app.filter('pagination', function() {
    return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
            var start = (currentPage-1)*pageSize;
            var end = currentPage*pageSize;
            return input.slice(start, end);
        }
    };
});

app.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);
app.filter('sum', function(){
    return function(items, prop){
        return items.reduce(function(a, b){
            return a + b[prop];
        }, 0);
    };
});



app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind("change", function (changeEvent) {
                var values = [];

                for (var i = 0; i < element[0].files.length; i++) {
                    var reader = new FileReader();

                    reader.onload = (function (i) {
                        return function(e) {
                            var value = {
                                lastModified: changeEvent.target.files[i].lastModified,
                                lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                name: changeEvent.target.files[i].name,
                                size: changeEvent.target.files[i].size,
                                type: changeEvent.target.files[i].type,
                                data: e.target.result
                            };
                            values.push(value);
                        }

                    })(i);

                    reader.readAsDataURL(changeEvent.target.files[i]);
                }


                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    }
}]);
