app.controller('AdministratorController', function($scope,$http,$anchorScroll,ngNotify,User){

    $scope.administratoredit=false;
    $scope.administrators=[];

    User.query({usertype:'Admin'},function(administrator){
        $scope.administrators=administrator;
    });

    $scope.newAdministrator=function(){
        $scope.administratoredit=true;
        $scope.newadministrator = new User();
        $scope.curAdministrator = {};
    };

    $scope.editAdministrator = function (thisAdministrator) {
        $scope.administratoredit = true;
        $scope.curAdministrator =  thisAdministrator;
        $scope.newadministrator = angular.copy(thisAdministrator);
        $anchorScroll();
    };

    $scope.addAdministrator = function () {
        $scope.newadministrator.usertype='Admin';
        if ($scope.curAdministrator.id) {
            $scope.newadministrator.$update(function(administrator){
                angular.extend($scope.curAdministrator, $scope.curAdministrator, administrator);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Administrator Updated Successfully');
            });
        } else{
            $scope.newadministrator.$save(function(administrator){
                $scope.administrators.push(administrator);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Administrator Saved Successfully');
            });
        }
        $scope.administratoredit = false;
        $scope.newadministrator = new User();
    };

    $scope.deleteAdministrator = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.administrators.indexOf(item);
                $scope.administrators.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Administrator Removed Successfully');
            });
        }
    };

    $scope.cancelAdministrator=function(){
        $scope.administratoredit=false;
        $scope.newadministrator = new User();
    };

    $scope.changeAdministratorStatus=function(administrator){
        $http.post('/api/changeUserStatus',{id:administrator.id}).
            success(function(data,status,headers,config){
                angular.extend(administrator,administrator,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});