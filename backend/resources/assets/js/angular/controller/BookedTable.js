app.controller('BookedTableController', function($scope,$http,$anchorScroll,ngNotify,BookedTable){

    $scope.bookedtableedit=false;
    $scope.bookedtables=[];

    BookedTable.query(function(bookedtable){
        $scope.bookedtables=bookedtable;
    });

    $scope.newBookedTable=function(){
        $scope.bookedtableedit=true;
        $scope.newbookedtable = new BookedTable();
        $scope.curBookedTable = {};
    };

    $scope.editBookedTable = function (thisBookedTable) {
        $scope.bookedtableedit = true;
        $scope.curBookedTable =  thisBookedTable;
        $scope.newbookedtable = angular.copy(thisBookedTable);
        $anchorScroll();
    };

    $scope.addBookedTable = function () {
        if ($scope.curBookedTable.id) {
            $scope.newbookedtable.$update(function(bookedtable){
                angular.extend($scope.curBookedTable, $scope.curBookedTable, bookedtable);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('BookedTable Updated Successfully');
            });
        } else{
            $scope.newbookedtable.$save(function(bookedtable){
                $scope.bookedtables.push(bookedtable);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('BookedTable Saved Successfully');
            });
        }
        $scope.bookedtableedit = false;
        $scope.newbookedtable = new BookedTable();
    };

    $scope.deleteBookedTable = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.bookedtables.indexOf(item);
                $scope.bookedtables.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('BookedTable Removed Successfully');
            });
        }
    };

    $scope.cancelBookedTable=function(){
        $scope.bookedtableedit=false;
        $scope.newbookedtable = new BookedTable();
    };

    $scope.changeBookedTableStatus=function(bookedtable){
        $http.post('/api/changeBookTableStatus',{id:bookedtable.id}).
            success(function(data,status,headers,config){
                angular.extend(bookedtable,bookedtable,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});