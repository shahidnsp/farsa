app.controller('ChangePasswordController', function($scope,$http,ngNotify){

    $scope.changePassword=function(oldpassword,newpassword,confirmpassword){
        $http.post('/api/changePassword',{oldpassword:oldpassword,newpassword:newpassword,confirmpassword:confirmpassword}).
            success(function(data,status,headers,config){
                $scope.user=data;
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Password Changed Successfully');
            }).error(function(data,status,headers,config){
                console.log(data);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'error',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Invalid Information..Try Again');
            });
    }
});