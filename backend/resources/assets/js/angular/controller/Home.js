app.controller('HomeController', function($scope,$http,$interval,$location){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    $scope.name="";


    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
    /* Nav menu */


    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "active" : "";
    };

    $http.get('/api/getMyProfile').
        success(function(data,status,headers,config){
            $scope.user=data;
        }).error(function(data,status,headers,config){
            console.log(data);
        });

    getOrder();
    var theInterval = $interval(function(){
        getOrder();
    }.bind(this), 40000);

    $scope.$on('$destroy', function () {
        $interval.cancel(theInterval)
    });

    function getOrder(){
        $http.get('/api/getDashboardInfo').
            success(function(data,status,headers,config){
                $scope.info=data;
                if(data.pending_orders.length!=0){
                    var audio = new Audio('img/to-the-point.mp3');
                    audio.play();
                }
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});