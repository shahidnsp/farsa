app.controller('MenuController', function($scope,$http,$anchorScroll,ngNotify,Menu){

    $scope.menuedit=false;
    $scope.menus=[];

    Menu.query(function(menu){
        $scope.menus=menu;
    });

    $scope.newMenu=function(){
        $scope.menuedit=true;
        $scope.newmenu = new Menu();
        $scope.curMenu = {};
    };

    $scope.editMenu = function (thisMenu) {
        $scope.menuedit = true;
        $scope.curMenu =  thisMenu;
        $scope.newmenu = angular.copy(thisMenu);
        $anchorScroll();
    };

    $scope.addMenu = function () {
        if ($scope.curMenu.id) {
            $scope.newmenu.$update(function(menu){
                angular.extend($scope.curMenu, $scope.curMenu, menu);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Menu Updated Successfully');
            });
        } else{
            $scope.newmenu.$save(function(menu){
                $scope.menus.push(menu);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Menu Saved Successfully');
            });
        }
        $scope.menuedit = false;
        $scope.newmenu = new Menu();
    };

    $scope.deleteMenu = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.menus.indexOf(item);
                $scope.menus.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Menu Removed Successfully');
            });
        }
    };

    $scope.cancelMenu=function(){
        $scope.menuedit=false;
        $scope.newmenu = new Menu();
    };

    $scope.changeMenuStatus=function(menu){
        $http.post('/api/changeMenuStatus',{id:menu.id}).
            success(function(data,status,headers,config){
                angular.extend(menu,menu,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});