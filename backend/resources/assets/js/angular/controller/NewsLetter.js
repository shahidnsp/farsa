app.controller('NewsLetterController', function($scope,$http,$anchorScroll,ngNotify,NewsLetter){

    $scope.newsLetteredit=false;
    $scope.newsLetters=[];

    NewsLetter.query(function(newsLetter){
        $scope.newsLetters=newsLetter;
        console.log(newsLetter);
    });

    $scope.newNewsLetter=function(){
        $scope.newsLetteredit=true;
        $scope.newnewsLetter = new NewsLetter();
        $scope.curNewsLetter = {};
    };

    $scope.editNewsLetter = function (thisNewsLetter) {
        $scope.newsLetteredit = true;
        $scope.curNewsLetter =  thisNewsLetter;
        $scope.newnewsLetter = angular.copy(thisNewsLetter);
        $anchorScroll();
    };

    $scope.addNewsLetter = function () {

        if ($scope.curNewsLetter.id) {
            $scope.newnewsLetter.$update(function(newsLetter){
                angular.extend($scope.curNewsLetter, $scope.curNewsLetter, newsLetter);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('NewsLetter Updated Successfully');
            });
        } else{
            $scope.newnewsLetter.$save(function(newsLetter){
                $scope.newsLetters.push(newsLetter);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('NewsLetter Saved Successfully');
            });
        }
        $scope.newsLetteredit = false;
        $scope.newnewsLetter = new NewsLetter();
    };

    $scope.deleteNewsLetter = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.newsLetters.indexOf(item);
                $scope.newsLetters.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('NewsLetter Removed Successfully');
            });
        }
    };

    $scope.cancelNewsLetter=function(){
        $scope.newsLetteredit=false;
        $scope.newnewsLetter = new NewsLetter();
    };

    $scope.changeNewsLetterStatus=function(newsLetter){
        $http.post('/api/changeNewsLetterStatus',{id:newsLetter.id}).
            success(function(data,status,headers,config){
                angular.extend(newsLetter,newsLetter,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});