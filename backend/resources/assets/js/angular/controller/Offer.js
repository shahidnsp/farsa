app.controller('OfferController', function($scope,$http,$anchorScroll,ngNotify,Offer,Product){

    $scope.offeredit=false;
    $scope.offers=[];

    $scope.products=[];

    Product.query(function(product){
        $scope.products=product;
    });

    Offer.query(function(offer){
        $scope.offers=offer;
    });

    $scope.newOffer=function(){
        $scope.offeredit=true;
        $scope.newoffer = new Offer();
        $scope.curOffer = {};
    };

    $scope.editOffer = function (thisOffer) {
        $scope.offeredit = true;
        $scope.curOffer =  thisOffer;
        $scope.newoffer = angular.copy(thisOffer);
        $anchorScroll();
    };

    $scope.addOffer = function () {
        if ($scope.curOffer.id) {
            $scope.newoffer.$update(function(offer){
                angular.extend($scope.curOffer, $scope.curOffer, offer);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Offer Updated Successfully');
            });
        } else{
            $scope.newoffer.$save(function(offer){
                $scope.offers.push(offer);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Offer Saved Successfully');
            });
        }
        $scope.offeredit = false;
        $scope.newoffer = new Offer();
    };

    $scope.deleteOffer = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.offers.indexOf(item);
                $scope.offers.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Offer Removed Successfully');
            });
        }
    };

    $scope.cancelOffer=function(){
        $scope.offeredit=false;
        $scope.newoffer = new Offer();
    };

    $scope.changeOfferStatus=function(offer){
        $http.post('/api/changeOfferStatus',{id:offer.id}).
            success(function(data,status,headers,config){
                angular.extend(offer,offer,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});