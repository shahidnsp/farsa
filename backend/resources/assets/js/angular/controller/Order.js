app.controller('OrderController', function($scope,$http,$anchorScroll,ngNotify,Order,User,WeekendOffer,Address){

    $scope.orderedit=false;
    $scope.orders=[];

    $scope.users=[];

    $scope.fromDate=new Date();
    $scope.toDate=new Date();

    User.query({usertype:'User'},function(user){
        $scope.users=user;
    });

    $scope.weekendOffers=[];

    WeekendOffer.query(function(weekendOffer){
        $scope.weekendOffers=weekendOffer;
    });

    $scope.addresses=[];

    Address.query(function(address){
        $scope.addresses=address;
    });
    


    Order.query(function(order){
        $scope.orders=order;
    });

    $scope.newOrder=function(){
        $scope.orderedit=true;
        $scope.neworder = new Order();
        $scope.curOrder = {};
    };

    $scope.editOrder = function (thisOrder) {
        $scope.orderedit = true;
        $scope.curOrder =  thisOrder;
        $scope.neworder = angular.copy(thisOrder);
        $anchorScroll();
    };

    $scope.addOrder = function () {
        if ($scope.curOrder.id) {
            $scope.neworder.$update(function(order){
                angular.extend($scope.curOrder, $scope.curOrder, order);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Order Updated Successfully');
            });
        } else{
            $scope.neworder.$save(function(order){
                $scope.orders.push(order);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Order Saved Successfully');
            });
        }
        $scope.orderedit = false;
        $scope.neworder = new Order();
    };

    $scope.deleteOrder = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.orders.indexOf(item);
                $scope.orders.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Order Removed Successfully');
            });
        }
    };

    $scope.cancelOrder=function(){
        $scope.orderedit=false;
        $scope.neworder = new Order();
    };

    $scope.changeOrderStatus=function(order){
        $http.post('/api/changeOrderStatus',{id:order.id}).
            success(function(data,status,headers,config){
                angular.extend(order,order,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.search=function(fromDate,toDate){
        Order.query({fromDate:fromDate,toDate:toDate},function(order){
            $scope.orders=order;
        });
    };


});