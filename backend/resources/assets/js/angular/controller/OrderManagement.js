app.controller('OrderManagementController', function($scope,$http,$anchorScroll,$ngConfirm,ngNotify,Order,User,WeekendOffer,Address){

    $scope.orders=[];

    $scope.fromDate=new Date();
    $scope.toDate=new Date();

    $scope.totalAmount=0;


    /*Order.query({paymentstatus:1},function(order){
        $scope.orders=order;
    });*/

    $scope.search=function(fromDate,toDate){
        Order.query({fromDate:fromDate,toDate:toDate,paymentstatus:1},function(order){
            $scope.orders=order;
            getSum(order);
        });
    };

    function getSum(orders){
        var length=orders.length;
        var sum=0;
        for(i=0;i<length;i++){
            sum+=parseFloat(orders[i].grandtotal);
        }
        $scope.totalAmount=sum;
    }

    //$scope.search($scope.fromDate,$scope.toDate,true);

    Order.query({paymentstatus:1,status:true},function(order){
        $scope.orders=order;
        getSum(order);
    });

    $scope.getPdf=function(fromDate,toDate){
        var req = {
            method: 'POST',
            url: 'api/getOrderPdf',
            responseType:"arraybuffer",
            data: {fromDate:fromDate,toDate:toDate}
        };

        $http(req)
            .success(function(data, status, headers){
                var arr = data;
                var byteArray = new Uint8Array(arr);
                var a = window.document.createElement('a');

                a.href = window.URL.createObjectURL(
                    new Blob([byteArray], { type: 'application/octet-stream' })
                );
                a.download ='Orders'+new Date()+'.pdf' ;//headers('filename');

                // Append anchor to body.
                document.body.appendChild(a);
                a.click();


                // Remove anchor from body
                document.body.removeChild(a);

                $scope.showProgress=false;
            }
        ).error(function(data, status, headers){
                console.log(data);
            }
        );
    };

    $scope.changeStatus=function(order){

        $ngConfirm({
            title: 'Confirm...................!',
            content: 'Do you really need to Change Status ?',
            //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
            scope: $scope,
            theme: 'dark',
            buttons: {
                // long hand button definition
                ok: {
                    text: "Yes!",
                    btnClass: 'btn-primary',
                    keys: ['enter'], // will trigger when enter is pressed
                    action: function(scope){
                        $http.post('/api/changeOrderStatus',{id:order.id}).
                            success(function(data,status,headers,config){
                                angular.extend(order,order,data);
                            }).error(function(data,status,headers,config){
                                console.log(data);
                            });
                    }
                },
                // short hand button definition
                close: function(scope){
                    //$ngConfirm('the user clicked close');
                }
            }
        });

        $scope.search($scope.fromDate,$scope.toDate);
    };

});