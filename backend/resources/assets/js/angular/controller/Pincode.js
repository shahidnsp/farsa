app.controller('PincodeController', function($scope,$http,$anchorScroll,ngNotify,Pincode){

    $scope.pincodeedit=false;
    $scope.pincodes=[];

    Pincode.query(function(pincode){
        $scope.pincodes=pincode;
    });

    $scope.newPincode=function(){
        $scope.pincodeedit=true;
        $scope.newpincode = new Pincode();
        $scope.curPincode = {};
    };

    $scope.editPincode = function (thisPincode) {
        $scope.pincodeedit = true;
        $scope.curPincode =  thisPincode;
        $scope.newpincode = angular.copy(thisPincode);
        $anchorScroll();
    };

    $scope.addPincode = function () {
        if ($scope.curPincode.id) {
            $scope.newpincode.$update(function(pincode){
                angular.extend($scope.curPincode, $scope.curPincode, pincode);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Pincode Updated Successfully');
            });
        } else{
            $scope.newpincode.$save(function(pincode){
                $scope.pincodes.push(pincode);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Pincode Saved Successfully');
            });
        }
        $scope.pincodeedit = false;
        $scope.newpincode = new Pincode();
    };

    $scope.deletePincode = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.pincodes.indexOf(item);
                $scope.pincodes.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Pincode Removed Successfully');
            });
        }
    };

    $scope.cancelPincode=function(){
        $scope.pincodeedit=false;
        $scope.newpincode = new Pincode();
    };

    $scope.changePincodeStatus=function(pincode){
        $http.post('/api/changePincodeStatus',{id:pincode.id}).
            success(function(data,status,headers,config){
                angular.extend(pincode,pincode,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }
    
});