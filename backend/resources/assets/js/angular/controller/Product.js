app.controller('ProductController', function($scope,$http,$anchorScroll,ngNotify,Product,Menu){

    $scope.productedit=false;
    $scope.products=[];

    Product.query(function(product){
        $scope.products=product;
    });

    Menu.query(function(menu){
        $scope.menues=menu;
    });

    $scope.newProduct=function(){
        $scope.productedit=true;
        $scope.newproduct = new Product();
        $scope.newproduct.prices=[{"name": "","amount":""}];
        $scope.curProduct = {};
    };

    $scope.editProduct = function (thisProduct) {
        $scope.productedit = true;
        $scope.curProduct =  thisProduct;
        $scope.newproduct = angular.copy(thisProduct);

        $http.post('/api/getProductPriceList',{id:thisProduct.id}).
            success(function(data,status,headers,config){
                $scope.newproduct.prices=data;
            }).error(function(data,status,headers,config){
                console.log(data);
            });


        $anchorScroll();
    };

    $scope.addProduct = function () {
        if ($scope.curProduct.id) {
            $scope.newproduct.$update(function(product){
                angular.extend($scope.curProduct, $scope.curProduct, product);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Product Updated Successfully');
            });
        } else{
            $scope.newproduct.$save(function(product){
                $scope.products.push(product);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Product Saved Successfully');
            });
        }
        $scope.productedit = false;
        $scope.newproduct = new Product();
    };

    $scope.dynamicField = function (buttonStatus, inputIndex) {
        if (buttonStatus) {
            $scope.newproduct.prices.push({"name": "","amount":""});
        } else {
            $scope.newproduct.prices.splice(inputIndex, 1);
        }
    };


    $scope.deleteProduct = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.products.indexOf(item);
                $scope.products.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Product Removed Successfully');
            });
        }
    };

    $scope.cancelProduct=function(){
        $scope.productedit=false;
        $scope.newproduct = new Product();
    };

    $scope.changeProductStatus=function(product){
        $http.post('/api/changeProductStatus',{id:product.id}).
            success(function(data,status,headers,config){
                angular.extend(product,product,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});