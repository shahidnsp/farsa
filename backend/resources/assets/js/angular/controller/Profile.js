app.controller('ProfileController', function($scope,$http,ngNotify){

    $scope.updateProfile=function(name,email,photos){
        $http.post('/api/updateMyProfile',{name:name,email:email,photos:photos}).
            success(function(data,status,headers,config){
                $scope.user=data;
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Profile Updated Successfully');
            }).error(function(data,status,headers,config){
                console.log(data);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'error',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Invalid Information or Email already taken');
            });
    }
});