app.controller('SliderController', function($scope,$http,$anchorScroll,ngNotify,Product,Slider){

    $scope.slideredit=false;
    $scope.sliders=[];

    Slider.query(function(slider){
        $scope.sliders=slider;
    });

    $scope.products=[];

    Product.query(function(product){
        $scope.products=product;
    });

    $scope.newSlider=function(){
        $scope.slideredit=true;
        $scope.newslider = new Slider();
        $scope.curSlider = {};
    };

    $scope.editSlider = function (thisSlider) {
        $scope.slideredit = true;
        $scope.curSlider =  thisSlider;
        $scope.newslider = angular.copy(thisSlider);
        $anchorScroll();
    };

    $scope.addSlider = function () {
        if ($scope.curSlider.id) {
            $scope.newslider.$update(function(slider){
                angular.extend($scope.curSlider, $scope.curSlider, slider);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Updated Successfully');
            });
        } else{
            $scope.newslider.$save(function(slider){
                $scope.sliders.push(slider);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Saved Successfully');
            });
        }
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

    $scope.deleteSlider = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.sliders.indexOf(item);
                $scope.sliders.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Removed Successfully');
            });
        }
    };

    $scope.cancelSlider=function(){
        $scope.slideredit=false;
        $scope.newslider = new Slider();
    };

    $scope.changeSliderStatus=function(slider){
        $http.post('/api/changeSliderStatus',{id:slider.id}).
            success(function(data,status,headers,config){
                angular.extend(slider,slider,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});