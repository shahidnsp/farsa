app.controller('TestimonialController', function($scope,$http,$anchorScroll,ngNotify,Testimonial){

    $scope.testimonialedit=false;
    $scope.testimonials=[];

    Testimonial.query(function(testimonial){
        $scope.testimonials=testimonial;
    });

    $scope.newTestimonial=function(){
        $scope.testimonialedit=true;
        $scope.newtestimonial = new Testimonial();
        $scope.curTestimonial = {};
    };

    $scope.editTestimonial = function (thisTestimonial) {
        $scope.testimonialedit = true;
        $scope.curTestimonial =  thisTestimonial;
        $scope.newtestimonial = angular.copy(thisTestimonial);
        $anchorScroll();
    };

    $scope.addTestimonial = function () {
        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function(testimonial){
                angular.extend($scope.curTestimonial, $scope.curTestimonial, testimonial);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });
        } else{
            $scope.newtestimonial.$save(function(testimonial){
                $scope.testimonials.push(testimonial);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.deleteTestimonial = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.testimonials.indexOf(item);
                $scope.testimonials.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Removed Successfully');
            });
        }
    };

    $scope.cancelTestimonial=function(){
        $scope.testimonialedit=false;
        $scope.newtestimonial = new Testimonial();
    };

    $scope.changeTestimonialStatus=function(testimonial){
        $http.post('/api/changeTestimonialStatus',{id:testimonial.id}).
            success(function(data,status,headers,config){
                angular.extend(testimonial,testimonial,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});