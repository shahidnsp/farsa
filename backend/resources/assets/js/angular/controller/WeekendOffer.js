app.controller('WeekendOfferController', function($scope,$http,$anchorScroll,ngNotify,WeekendOffer){

    $scope.weekendOfferedit=false;
    $scope.weekendOffers=[];

    WeekendOffer.query(function(weekendOffer){
        $scope.weekendOffers=weekendOffer;
    });

    $scope.newWeekendOffer=function(){
        $scope.weekendOfferedit=true;
        $scope.newweekendOffer = new WeekendOffer();
        $scope.curWeekendOffer = {};
    };

    $scope.editWeekendOffer = function (thisWeekendOffer) {
        $scope.weekendOfferedit = true;
        $scope.curWeekendOffer =  thisWeekendOffer;
        $scope.newweekendOffer = angular.copy(thisWeekendOffer);
        $anchorScroll();
    };

    $scope.addWeekendOffer = function () {
        if ($scope.curWeekendOffer.id) {
            $scope.newweekendOffer.$update(function(weekendOffer){
                angular.extend($scope.curWeekendOffer, $scope.curWeekendOffer, weekendOffer);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Weekend Offer Updated Successfully');
            });
        } else{
            $scope.newweekendOffer.$save(function(weekendOffer){
                $scope.weekendOffers.push(weekendOffer);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('WeekendOffer Saved Successfully');
            });
        }
        $scope.weekendOfferedit = false;
        $scope.newweekendOffer = new WeekendOffer();
    };

    $scope.deleteWeekendOffer = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.weekendOffers.indexOf(item);
                $scope.weekendOffers.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Weekend Offer Removed Successfully');
            });
        }
    };

    $scope.cancelWeekendOffer=function(){
        $scope.weekendOfferedit=false;
        $scope.newweekendOffer = new WeekendOffer();
    };

    $scope.changeWeekendOfferStatus=function(weekendOffer){
        $http.post('/api/changeWeekendOfferStatus',{id:weekendOffer.id}).
            success(function(data,status,headers,config){
                angular.extend(weekendOffer,weekendOffer,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }


});