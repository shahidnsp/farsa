/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('AddressService',[]).factory('Address',['$resource',
    function($resource){
        return $resource('/api/address/:addressId',{
            addressId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);