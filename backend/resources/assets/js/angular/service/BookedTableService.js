/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('BookedTableService',[]).factory('BookedTable',['$resource',
    function($resource){
        return $resource('/api/bookedtables/:bookedtablesId',{
            bookedtablesId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);