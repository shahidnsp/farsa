/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('MenuService',[]).factory('Menu',['$resource',
    function($resource){
        return $resource('/api/menu/:menuId',{
            menuId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);