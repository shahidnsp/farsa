/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('OfferService',[]).factory('Offer',['$resource',
    function($resource){
        return $resource('/api/offer/:offerId',{
            offerId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);