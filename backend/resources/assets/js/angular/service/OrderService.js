/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('OrderService',[]).factory('Order',['$resource',
    function($resource){
        return $resource('/api/order/:orderId',{
            orderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);