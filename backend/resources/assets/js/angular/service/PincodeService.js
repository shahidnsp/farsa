/**
 * Created by Shahid Neermunda on 2/3/18.
 */

angular.module('PincodeService',[]).factory('Pincode',['$resource',
    function($resource){
        return $resource('/api/pincode/:pincodeId',{
            pincodeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);