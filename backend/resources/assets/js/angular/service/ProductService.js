/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('ProductService',[]).factory('Product',['$resource',
    function($resource){
        return $resource('/api/product/:productId',{
            productId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);