/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('SliderService',[]).factory('Slider',['$resource',
    function($resource){
        return $resource('/api/slider/:sliderId',{
            sliderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);