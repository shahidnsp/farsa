/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('WeekendOfferService',[]).factory('WeekendOffer',['$resource',
    function($resource){
        return $resource('/api/weekendoffer/:weekendofferId',{
            weekendofferId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);