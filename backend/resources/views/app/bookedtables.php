<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>BookedTables</h2>

    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="bookedtableedit" ng-click="newBookedTable()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="bookedtableedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New BookedTable
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addBookedTable();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-11">
                            <input ng-model="newbookedtable.name" type="text" placeholder="Name" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Email</label>
                        <div class="col-sm-11">

                            <input ng-model="newbookedtable.email"  placeholder="Email" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Phone</label>
                        <div class="col-sm-11">
                            <input ng-model="newbookedtable.phone"  placeholder="Phone" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">DateTime</label>
                        <div class="col-sm-11">
                            <input ng-model="newbookedtable.datetime"  placeholder="Datetime" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Attendance</label>
                        <div class="col-sm-11">
                            <input ng-model="newbookedtable.attendances"  placeholder="attendance" class="form-control" required="">
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelBookedTable();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Datetime</th>
                                    <th>Attendance</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="bookedtable in listCount  = (bookedtables | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{bookedtable.name}}</td>
                                    <td>{{bookedtable.email}}</td>
                                    <td>{{bookedtable.phone}}</td>
                                    <td>{{bookedtable.datetime}}</td>
                                    <td>{{bookedtable.attendances}}</td>
                                    <td>
                                        <button ng-if="bookedtable.notified==1" type="button" class="btn btn-success" ng-click="changeBookedTableStatus(bookedtable);">
                                            Active
                                        </button>
                                        <button ng-if="bookedtable.notified==0" type="button" class="btn btn-info" ng-click="changeBookedTableStatus(bookedtable);">
                                            InActive
                                        </button>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editBookedTable(bookedtable);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" ng-click="deleteBookedTable(bookedtable);">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="bookedtables.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>