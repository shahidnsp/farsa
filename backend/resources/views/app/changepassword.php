<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>My Profile</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                Change My Password
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="changePassword(oldpassword,newpassword,confirmpassword);">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Current Password</label>
                        <div class="col-sm-5">
                            <input ng-model="oldpassword" type="password" placeholder="Current Password" class="form-control" required="">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-5">
                            <input ng-model="newpassword" type="password" placeholder="New Password" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-5">
                            <input ng-model="confirmpassword" type="password" placeholder="Confirm Password" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-w-m btn-primary">Change Password</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>
