
<!DOCTYPE html>
<html ng-app="myApp">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Farsa Restaurant | Dashboard</title>

    <link href="css/app.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">


</head>

<body ng-controller="HomeController">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" style="width: 60px;" src="images/@{{user.photo}}" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">@{{user.name}}</strong>
                             </span> <span class="text-muted text-xs block">My Profile <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#/profile">Profile</a></li>
                                <li><a href="#/changepassword">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li ng-class="menuClass('dashboard')"><a href="#/dashboard">Dashboard</a></li>
                            <li ng-class="menuClass('dashboard')"><a href="/stats" target="_blank">Web Analytics</a></li>
                        </ul>
                    </li>
                    <li ng-class="menuClass('clients')">
                        <a href="#/clients"><i class="fa fa-users"></i> <span class="nav-label">Clients</span></a>
                    </li>
                    <li ng-class="menuClass('administrator')">
                        <a href="#/administrator"><i class="fa fa-users"></i> <span class="nav-label">Administrator</span></a>
                    </li>
                    <li ng-class="menuClass('slider')">
                        <a href="#/pincode"><i class="fa fa-sliders"></i> <span class="nav-label">Pincode</span></a>
                    </li>
                    <li ng-class="menuClass('slider')">
                        <a href="#/slider"><i class="fa fa-sliders"></i> <span class="nav-label">Slider</span></a>
                    </li>
                    <li ng-class="menuClass('testimonial')">
                        <a href="#/testimonial"><i class="fa fa-commenting"></i> <span class="nav-label">Testimonial</span></a>
                    </li>
                    <li ng-class="menuClass('menu')">
                        <a href="#/menu"><i class="fa fa-bars"></i> <span class="nav-label">Food Menu</span></a>
                    </li>
                    <li ng-class="menuClass('product')">
                        <a href="#/product"><i class="fa fa-cutlery"></i> <span class="nav-label">Product</span></a>
                    </li>
                    <li ng-class="menuClass('weekendoffer')">
                        <a href="#/weekendoffer"><i class="fa fa-gift"></i> <span class="nav-label">Weekend Offer</span></a>
                    </li>
                    <li ng-class="menuClass('offers')">
                        <a href="#/offers"><i class="fa fa-gift"></i> <span class="nav-label">Offers</span></a>
                    </li>
                     <li ng-class="menuClass('bookedtables')">
                        <a href="#/bookedtables"><i class="fa fa-table"></i> <span class="nav-label">Booked Tables</span></a>
                    </li>
                     <li ng-class="menuClass('order')">
                        <a href="#/order"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Order</span></a>
                    </li>
                    <li ng-class="menuClass('ordermanagement')">
                        <a href="#/ordermanagement"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Order Management</span></a>
                    </li>

                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary "><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to Farsa Restaurant</span>
                        </li>

                        <li>
                            <form id="myform" method="post" action="{{url('logout')}}">
                              {{ csrf_field() }}
                              <a onclick="document.getElementById('myform').submit();"><i class="fa fa-sign-out"></i> Log out</a>
                            </form>
                           <!-- <a href="login.html">


                            </a>-->
                        </li>
                        <li>
                            <a class="right-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div ng-view></div>

             <div id="small-chat">

            <span class="badge badge-warning pull-right">5</span>
            <a class="open-small-chat">
                <i class="fa fa-comments"></i>

            </a>
        </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.metisMenu.js"></script>
    <script src="js/jquery.slimscroll.min.js"></script>


    <!-- Custom and plugin javascript -->
    {{--<script src="js/inspinia.js"></script>--}}
    <script src="js/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/app.js"></script>



</body>
</html>
