<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Orders</h2>

    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="orderedit" ng-click="newOrder()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="orderedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New Order
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addOrder();">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date Time</label>
                        <div class="col-sm-4">
                            <input ng-model="neworder.datetime" type="text" placeholder="Datetime" class="form-control" required="">
                        </div>

                        <label class="col-sm-1 control-label">User</label>
                        <div class="col-sm-5">
                            <select ng-model="neworder.users_id" class="form-control" required="">
                                <option value="">Select</option>
                                <option ng-repeat="user in users" ng-selected="user.id==neworder.users_id" value="{{user.id}}">{{user.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Total</label>
                        <div class="col-sm-10">
                            <input ng-model="neworder.total" type="text" placeholder="Total" class="form-control" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">GST</label>
                        <div class="col-sm-10">
                            <input ng-model="neworder.gst"  placeholder="GST" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Delivery Charge</label>
                        <div class="col-sm-10">
                            <input ng-model="neworder.deliverycharge"  placeholder="Delivery Charge" class="form-control">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Grand Total</label>
                        <div class="col-sm-10">
                            <input ng-model="neworder.grandtotal"  placeholder="Grand Total" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-4">
                            <select ng-model="neworder.addresses_id" class="form-control" required="">
                                <option value="">Select</option>
                                <option ng-repeat="address in addresses" ng-selected="address.id==neworder.addresses_id" value="{{address.id}}">{{address.name}}</option>
                            </select>
                        </div>

                        <label class="col-sm-2 control-label">Weekend offer</label>
                        <div class="col-sm-4">
                            <select ng-model="neworder.weekend_offers_id" class="form-control" required="">
                                <option value="">Select</option>
                                <option ng-repeat="weekendoffer in weekendOffers" ng-selected="weekendoffer.id==neworder.weekend_offers_id" value="{{weekendoffer.id}}">{{weekendoffer.day}} | {{weekendoffer.amount}}</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelOrder();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="row form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3">From Date:</label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label for="date-picker-from" class="input-group-addon btn"><span class="fa fa-calendar"></span></label>
                                        <input id="date-picker-from" ng-model="fromDate"  type="text" class="date-picker form-control" />
                                    </div>
                                </div>
                                <label class="control-label col-sm-1">To Date:</label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label for="date-picker-to" class="input-group-addon btn"><span class="fa fa-calendar"></span></label>
                                        <input id="date-picker-to" ng-model="toDate"  type="text" class="date-picker form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button ng-click="search(fromDate,toDate);" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order No</th>
                                    <th>Datetime</th>
                                    <th>User</th>
                                    <th>Address</th>
                                    <th>Items</th>
                                    <th>Payment Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in listCount  = (orders | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{order.orderno}}</td>
                                    <td class="text-danger">{{order.datetime}}</td>
                                    <td>{{order.user.name}}</td>
                                    <td>
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr class="text-danger">
                                                    <td>Name:</td><td>{{order.address.name}}</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td>Phone:</td><td>{{order.address.phone}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Street:</td><td>{{order.address.street}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Landmark:</td><td>{{order.address.landmark}}</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td>Phone 2:</td><td>{{order.address.phone2}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email:</td><td>{{order.address.email}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Pincode:</td><td>{{order.address.pincode}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                               <tr>
                                                   <th>Item</th>
                                                   <th>Portion</th>
                                                   <th>Qty</th>
                                                   <th>Amount</th>
                                               </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="text-success" ng-repeat="item in order.orderitems">
                                                    <td>{{item.product.product.name}}</td>
                                                    <td>{{item.product.name}}</td>
                                                    <td>{{item.qty}}</td>
                                                    <td>{{item.amount}}</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td colspan="2">GST</td><td colspan="2">{{order.gst}}</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td colspan="2">Delivery Charge</td><td colspan="2">{{order.deliverycharge}}</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td colspan="2">Offer</td><td colspan="2">{{order.weekendoffer.amount}}({{order.weekendoffer.day}})</td>
                                                </tr>
                                                <tr class="text-danger">
                                                    <td colspan="2">Grand Total</td><td colspan="2">{{order.grandtotal}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary" ng-if="order.paymentstatus==1">Success</a>
                                        <a class="btn btn-sm btn-warning" ng-if="order.paymentstatus==0">Order Failed</a>
                                        <a class="btn btn-sm btn-danger" ng-if="order.paymentstatus==2">Payment Failed</a>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editOrder(order);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" ng-click="deleteOrder(order);">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="orders.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".date-picker").datepicker();

    $(".date-picker").on("change", function () {
        var id = $(this).attr("id");
        var val = $("label[for='" + id + "']").text();
        $("#msg").text(val + " changed");
    });
</script>