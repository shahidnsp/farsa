<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Order Management</h2>
    </div>
</div>


<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="row form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-2">From Date:</label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label for="date-picker-from" class="input-group-addon btn"><span class="fa fa-calendar"></span></label>
                                        <input id="date-picker-from" ng-model="fromDate"  type="text" class="date-picker form-control" />
                                    </div>
                                </div>
                                <label class="control-label col-sm-1">To Date:</label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <label for="date-picker-to" class="input-group-addon btn"><span class="fa fa-calendar"></span></label>
                                        <input id="date-picker-to" ng-model="toDate"  type="text" class="date-picker form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <button ng-click="search(fromDate,toDate);" class="btn btn-primary">Search</button>
                                    <button ng-click="getPdf(fromDate,toDate);" class="btn  btn-success">GET PENDING ORDER</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Address</th>
                                    <th>Items</th>
                                    <th>Delivery Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in listCount  = (orders | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">{{order.datetime}}</td>
                                    <td>
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td>Name:</td><td>{{order.address.name}}</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td>Phone:</td><td>{{order.address.phone}}</td>
                                            </tr>
                                            <tr>
                                                <td>Street:</td><td>{{order.address.street}}</td>
                                            </tr>
                                            <tr>
                                                <td>Landmark:</td><td>{{order.address.landmark}}</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td>Phone 2:</td><td>{{order.address.phone2}}</td>
                                            </tr>
                                            <tr>
                                                <td>Email:</td><td>{{order.address.email}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pincode:</td><td>{{order.address.pincode}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Portion</th>
                                                <th>Qty</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="text-success" ng-repeat="item in order.orderitems">
                                                <td>{{item.product.product.name}}</td>
                                                <td>{{item.product.name}}</td>
                                                <td>{{item.qty}}</td>
                                                <td>{{item.amount}}</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td colspan="2">GST</td><td colspan="2">{{order.gst}}</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td colspan="2">Delivery Charge</td><td colspan="2">{{order.deliverycharge}}</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td colspan="2">Offer</td><td colspan="2">{{order.weekendoffer.amount}}({{order.weekendoffer.day}})</td>
                                            </tr>
                                            <tr ng-class="order.deliverystatus=='Pending'?'text-danger':'text-success'">
                                                <td colspan="2">Grand Total</td><td colspan="2">{{order.grandtotal}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <select  ng-model="order.deliverystatus" ng-change="changeStatus(order);">
                                            <option value="Pending">Pending</option>
                                            <option value="Placed">Placed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span class="text-danger" style="font-size: 25px;">TOTAL AMOUNT:</span>
                                    </td>
                                    <td colspan="2">
                                        <span class="text-danger" style="font-size: 25px;">{{totalAmount}}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="orders.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".date-picker").datepicker();

    $(".date-picker").on("change", function () {
        var id = $(this).attr("id");
        var val = $("label[for='" + id + "']").text();
        $("#msg").text(val + " changed");
    });
</script>