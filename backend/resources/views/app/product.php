<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Products</h2>

        <!--<ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Forms</a>
            </li>
            <li class="active">
                <strong>PDF viewer</strong>
            </li>
        </ol>-->
    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="productedit" ng-click="newProduct()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="productedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New Product
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addProduct();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-4">
                            <input ng-model="newproduct.name" type="text" placeholder="Name" class="form-control" required="">
                        </div>
                        <label class="col-sm-1 control-label">Menu</label>
                        <div class="col-sm-6">
                            <select ng-model="newproduct.menues_id" class="form-control" required="">
                                <option value="">Select</option>
                                <option ng-repeat="menu in menues" value="{{menu.id}}">{{menu.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Description</label>
                        <div class="col-sm-11">
                            <textarea ng-model="newproduct.description"  placeholder="Content" class="form-control" required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Photo</label>
                        <div class="col-sm-11">
                            <input class="form-control" accept="image/*" ng-file-model="newproduct.photos" type="file" multiple/>
                        </div>
                    </div>
                    <div class="form-group" ng-repeat="price in newproduct.prices">
                        <label for="contact-number3" class="col-sm-1 control-label">Name {{$index + 1 }}</label>
                        <div class="col-sm-5">
                            <input type="text" ng-model="price.name"  placeholder="Name" class="form-control" required=""/>
                        </div>
                        <label for="contact-number3" class="col-sm-1 control-label">Price {{$index + 1 }}</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" ng-model="price.amount"  placeholder="Amount" class="form-control" required=""/>
                                <span class="input-group-btn">
                                  <button class="btn btn-default" type="button" ng-init="plusEmpSign = true" ng-click="plusEmpSign = !plusEmpSign; dynamicField($last, $index);">
                                      <i class="fa" ng-class="$last ? 'fa-plus' : 'fa-minus'"></i>
                                  </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelProduct();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Menu</th>
                                    <th>Description</th>
                                    <th>Photo</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="product in listCount  = (products | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{product.name}}</td>
                                    <td>{{product.menu.name}}</td>
                                    <td>{{product.description}}</td>
                                    <td>
                                        <img src="images/{{product.photo}}" class="img-responsive" style="width: 60px;height: 40px;" alt=""/>
                                    </td>
                                    <td>
                                        <button ng-if="product.active==1" type="button" class="btn btn-success" ng-click="changeProductStatus(product);">
                                            Active
                                        </button>
                                        <button ng-if="product.active==0" type="button" class="btn btn-info" ng-click="changeProductStatus(product);">
                                            InActive
                                        </button>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editProduct(product);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" ng-click="deleteProduct(product);">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="products.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>