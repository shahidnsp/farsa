<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>My Profile</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                Change My Profile
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="updateProfile(user.name,user.email,user.photos);">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-4">
                            <input ng-model="user.name" type="text" placeholder="Name" class="form-control" required="">
                        </div>
                        <label class="col-sm-2 control-label">Email/Username</label>
                        <div class="col-sm-5">
                            <input ng-model="user.email" type="email" placeholder="Email Address" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Photo</label>
                        <div class="col-sm-11">
                            <input class="form-control" accept="image/*" ng-file-model="user.photos" type="file" multiple/>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Update</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>
