<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Weekend Offers</h2>

    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="weekendOfferedit" ng-click="newWeekendOffer()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="weekendOfferedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New Weekend Offer
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addWeekendOffer();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Day</label>
                        <div class="col-sm-11">
                            <select ng-model="newweekendOffer.day" class="form-control" required="">
                                <option value="">Select</option>
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Amount</label>
                        <div class="col-sm-4">
                            <input ng-model="newweekendOffer.amount" type="text" placeholder="Discount %" class="form-control" required="">
                        </div>
                        <label class="col-sm-3 control-label">Applicable in(More than Amt)</label>
                        <div class="col-sm-4">
                            <input ng-model="newweekendOffer.gratherthanamount" type="text" placeholder="If Amount more than" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelWeekendOffer();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Day</th>
                                    <th>Discount %</th>
                                    <th>Applicable in (Amt)</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="weekendOffer in listCount  = (weekendOffers | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{weekendOffer.day}}</td>
                                    <td>{{weekendOffer.amount}}</td>
                                    <td>{{weekendOffer.gratherthanamount}}</td>
                                    <td>
                                        <button ng-if="weekendOffer.active==1" type="button" class="btn btn-success" ng-click="changeWeekendOfferStatus(weekendOffer);">
                                            Active
                                        </button>
                                        <button ng-if="weekendOffer.active==0" type="button" class="btn btn-info" ng-click="changeWeekendOfferStatus(weekendOffer);">
                                            InActive
                                        </button>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editWeekendOffer(weekendOffer);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" ng-click="deleteWeekendOffer(weekendOffer);">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="weekendOffers.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>