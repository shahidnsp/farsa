{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="utf-8">
<meta name="author" content="Psybo Technologies">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- Title -->
<title>SignUp | FARSA Restaurants</title>
<!-- Favicons -->
<link rel="shortcut icon" href="frontend/assets/img/favicon.png">
<link rel="apple-touch-icon" href="frontend/assets/img/favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="frontend/assets/img/favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="frontend/assets/img/favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="frontend/assets/img/favicon_152x152.png">
<!-- CSS Plugins -->
<link rel="stylesheet" href="frontend/assets/plugins/bootstrap/dist/css/bootstrap.min.css" />
<link rel="stylesheet" href="frontend/assets/plugins/slick-carousel/slick/slick.css" />
<link rel="stylesheet" href="frontend/assets/plugins/animate.css/animate.min.css" />
<link rel="stylesheet" href="frontend/assets/plugins/animsition/dist/css/animsition.min.css" />
<!-- CSS Icons -->
<link rel="stylesheet" href="frontend/assets/css/themify-icons.css" />
<link rel="stylesheet" href="frontend/assets/plugins/font-awesome/css/font-awesome.min.css" />
<!-- CSS Theme -->
<link id="theme" rel="stylesheet" href="frontend/assets/css/themes/theme-beige.min.css" />
</head>
<body>

<!-- Body Wrapper -->
<div id="body-wrapper" class="animsition">

    <!-- Header -->
    <header id="header" class="light">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Logo -->
                    <div class="module module-logo light">
                        <a href="#">
                            <img src="frontend/assets/img/logo.jpg" alt="" width="88">
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="module right">
                        <a href="{{ route('login') }}" class="btn btn-outline-secondary"><span>Existing User? Log in</span></a>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <!-- Header / End -->

    <!-- Content -->
    <div id="content">
        <!-- Section -->
        <section class="section section-lg bg-light login-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 push-lg-3">
                        <!-- Book a Table -->
                        <div class="utility-box">
                            <div class="utility-box-title bg-dark dark">
                                <div class="bg-image"><img src="frontend/assets/img/photos/modal-review.jpg" alt=""></div>
                                <div>
                                    <span class="icon icon-primary"><i class="ti ti-tag"></i></span>
                                    <h4 class="mb-0">Join Now</h4>
                                </div>
                            </div>
                            <form method="POST" action="{{ route('register') }}" id="booking-form" data-validate>
                                 {{ csrf_field() }}
                                <div class="utility-box-content">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Name:</label>
                                        <input type="text"  class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                         @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                         @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>E-mail:</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label>Password:</label>
                                        <input type="password" name="password" class="form-control" required>
                                         @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                         @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password:</label>
                                        <input type="password" name="password_confirmation" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox" required="">
                                        <label for="checkbox-1" class="checkbox-custom-label">I Agree <a href="forgot-top" target="_blank">Terms and Conditions</a></label>
                                    </div>
                                </div>
                                <button class="utility-box-btn btn btn-secondary btn-block btn-lg btn-submit" type="submit">
                                    <span class="description">Signup Now</span>
                                    <span class="success">
                                        <svg x="0px" y="0px" viewBox="0 0 32 32"><path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/></svg>
                                    </span>
                                    <span class="error">Try again...</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content / End -->
</div>

<!-- JS Plugins -->
<script src="frontend/assets/plugins/jquery/dist/jquery.min.js"></script>
<script src="frontend/assets/plugins/tether/dist/js/tether.min.js"></script>
<script src="frontend/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="frontend/assets/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="frontend/assets/plugins/jquery.appear/jquery.appear.js"></script>
<script src="frontend/assets/plugins/jquery.scrollto/jquery.scrollTo.min.js"></script>
<script src="frontend/assets/plugins/jquery.localscroll/jquery.localScroll.min.js"></script>
<script src="frontend/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="frontend/assets/plugins/skrollr/dist/skrollr.min.js"></script>
<script src="frontend/assets/plugins/animsition/dist/js/animsition.min.js"></script>
<!-- JS Core -->
<script src="frontend/assets/js/core.js"></script>
<!-- JS Stylewsitcher -->
<script src="frontend/styleswitcher/styleswitcher.js"></script>
<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>
</body>
</html>

