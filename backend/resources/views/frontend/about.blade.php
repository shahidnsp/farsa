@extends('frontend.layout.app')

@section('title', 'About Us | FARSA Restaurants')

@section('content')
     <!-- Page Title -->
            <div class="page-title border-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 push-lg-5">
                            <h1 class="mb-0">About Us</h1>
                            <h4 class="mb-0">Delivering auspicious treat to taste buds for more than 75 years</h4>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Section -->
            <section class="section section-bg-edge">
                <div class="image left bottom col-md-7">
                    <div class="bg-image" style='background-size: 65% 100%;background-position: inherit;'><img src="frontend/assets/img/bg-about.jpg" alt=""></div>
                </div>
                <div class="container">
                    <div class="col-lg-5 push-lg-5 col-md-9 push-md-3">
                        <div class="rate mb-5 rate-lg"><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star"></i></div>
                        <h2>The best food in Malabar!</h2>
                        <p class="lead">
                            Delivering auspicious treat to taste buds for more than 75 years, Farsa Restaurant is becoming a whole new quality eat-hub. Farsa starting small in the initial period, understanding the regional magic helped to continually amaze the customers with delicious foods.  The progressive graph of Farsa reflects its influence in the hearts of people.
                        </p>

                        <p>
                            Emphasizing the restaurant's varied food items, the motto says, Quality and Variety! We are committed to bringing the best of foods to our customers with superior grade and care. Watching foods fill your tummy makes us satisfied. We believe people are great when they have a superb meal. We try to make people great! Farsa focuses on the healthy picture.
                        </p>

                        <h6>MD of Farsa Group</h6>
                        <img src="frontend/assets/img/svg/sign.svg" alt="" class="mb-5">
                        <h4>What people say about Us?</h4>
                        <a href="reviews" class="btn btn-outline-primary"><span>Check our reviews</span></a>
                    </div>
                </div>
            </section>

            <!-- Section -->
            <section class="section section-lg dark bg-dark">
                <!-- BG Image -->
                <div class="bg-image bg-parallax"><img src="frontend/assets/img/bg-croissant.jpg" alt=""></div>
                <div class="container text-center">
                    <div class="col-lg-8 push-lg-2">
                        <h2 class="mb-3">Would you like to visit Us?</h2>
                        <h5 class="text-muted">Book a table even right now or make an online order!</h5>
                        <a href="/" class="btn btn-primary"><span>Order Online</span></a>
                        <a href="/contact" class="btn btn-outline-primary"><span>Book a Table</span></a>
                    </div>
                </div>
            </section>
@endsection

