@extends('frontend.layout.app')

@section('title', 'My Account | FARSA Restaurants')

@section('content')
     <section id="main" class="container mt-7">
         <div class="row">
             <div class="col-md-9">
                 <div id="start">
                     <div class="example-box">
                         <?php $address=\App\Address::where('users_id',\Illuminate\Support\Facades\Auth::id())->get()->last(); ?>
                         <div class="example-box-title">Account Details <a href="#" data-toggle="modal" data-target="#demoModal"><i class="ti ti-pencil-alt editble"></i></a></div>
                         @if($address!=null)
                         <table class="table table-hover">
                             <tbody>
                                 <tr>
                                     <th>Name</th>
                                     <td>{{$address->name}}</td>
                                 </tr>
                                 <tr>
                                     <th>Phone Number</th>
                                     <td>{{$address->phone}}</td>
                                 </tr>
                                 <tr>
                                     <th>Email Address</th>
                                     <td>{{$address->email}}</td>
                                 </tr>
                                 <tr>
                                     <th>Pincode</th>
                                     <td>{{$address->pincode}}</td>
                                 </tr>
                                 <tr>
                                     <th>Street and Locality</th>
                                     <td>{{$address->street}}</td>
                                 </tr>
                                 <tr>
                                     <th>Landmark</th>
                                     <td>{{$address->landmark}}</td>
                                 </tr>
                                 <tr>
                                     <th>Alternate Phone</th>
                                     <td>{{$address->phone2}}</td>
                                 </tr>
                               {{--  <tr>
                                     <th>Delivery Address-1</th>
                                     <td>Psybo Technologies, V2 Tower Padikkad Road Manjeri Near Old Bustand <i class="ti ti-trash editble2"></i></td>
                                 </tr>
                                 <tr>
                                     <th>Delivery Address-2</th>
                                     <td>Psybo Technologies, V2 Tower Padikkad Road Manjeri Near Old Bustand <i class="ti ti-trash editble2"></i></td>
                                 </tr>--}}
                             </tbody>
                         </table>
                         @else
                            <br/>
                            <div class="alert alert-warning" role="alert">
                              Hai, {{\Illuminate\Support\Facades\Auth::user()->name}}, No Delivery Address found..!! Create it now <a href="#" data-toggle="modal" data-target="#demoModal"><i class="ti ti-pencil-alt editble"></i></a>
                            </div>
                         @endif
                     </div>
                 </div>
                 <div id="changepassword">
                     <div class="example-box">
                         <div class="example-box-title">Change Your Password</div>
                         <div class="row p-20">
                             <div class="col-md-6">
                                <form action="{{route('changeMyPassword')}}" method="post">
                                      {{ csrf_field() }}
                                     <div class="form-group">
                                         <label>Old Password:</label>
                                         <input name="oldpassword" type="password" class="form-control" required="">
                                     </div>
                                     <div class="form-group">
                                         <label>New Password:</label>
                                         <input id="password" name="newpassword" type="password"  class="form-control" required="">
                                     </div>
                                     <div class="form-group">
                                         <label>Confirm Password:</label>
                                         <input id="confirm_password" name="confirmpassword" type="password" class="form-control" required="">
                                     </div>
                                     <button type="submit" class="btn btn-outline-primary float-right" id="submit"><span>Update Changes</span></button>
                                </form>
                             </div>
                         </div>
                     </div>

                     <script>
                       var password = document.getElementById("password")
                         , confirm_password = document.getElementById("confirm_password");

                       function validatePassword(){
                         if(password.value != confirm_password.value) {
                           confirm_password.setCustomValidity("Passwords Don't Match");
                         } else {
                           confirm_password.setCustomValidity('');
                         }
                       }

                       password.onchange = validatePassword;
                       confirm_password.onkeyup = validatePassword;
                     </script>
                 </div>
             </div>
             <div class="col-md-3 hidden-sm hidden-xs">
                 <nav id="side-navigation" class="stick-to-content pt-5" data-local-scroll>
                     <ul class="nav nav-vertical">
                         <li class="nav-item"><a class="nav-link" href="#start"><span>Account Details</span></a></li>
                         <li class="nav-item"><a class="nav-link" href="#changepassword"><span>Change Your Password</span></a></li>
                         <li class="nav-item"><a class="nav-link" href="/myorders"><span>Order History</span></a></li>
                         <li class="nav-item" style="cursor: pointer;">
                            <form id="myform" method="post" action="{{url('logout')}}">
                                  {{ csrf_field() }}
                                  <a class="nav-link" onclick="document.getElementById('myform').submit();">Logout</a>
                            </form>
                          </li>
                     </ul>
                 </nav>
             </div>
         </div>
     </section>

     <!-- Modal / Account Details -->
     <div class="modal fade" id="demoModal" role="dialog">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="myModalLabel">Account Details</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
                 </div>
                 <form action="{{route('submitMyDeliveryAddress')}}" method="post">
                     <div class="modal-body">
                          {{ csrf_field() }}
                         <div class="form-group">
                             <label>Name:</label>
                             <input type="text" name="name" value="@if($address!=null){{$address->name}}@endif" class="form-control" required="">
                         </div>
                         <div class="form-group">
                             <label>Phone number:</label>
                             <input type="text" name="phone" value="@if($address!=null){{$address->phone}}@endif" class="form-control" required="">
                         </div>
                         <div class="form-group">
                             <label>Pincode:</label>
                             <input type="text" name="pincode" value="@if($address!=null){{$address->pincode}}@endif" class="form-control">
                         </div>
                         <div class="form-group">
                             <label>Street and Locality:</label>
                             <input type="text" name="street" value="@if($address!=null){{$address->street}}@endif" class="form-control" required="">
                         </div>
                         <div class="form-group">
                             <label>Landmark:</label>
                             <input type="text" name="landmark" value="@if($address!=null){{$address->landmark}}@endif" class="form-control" required="">
                         </div>
                         <div class="form-group">
                             <label>Alternate Phone(Optional):</label>
                             <input type="text" name="phone2" value="@if($address!=null){{$address->phone2}}@endif" class="form-control">
                         </div>
                          <div class="form-group">
                              <label>Email:</label>
                              <input type="text" name="email" value="@if($address!=null){{$address->email}}@endif" class="form-control">
                          </div>
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-dark" data-dismiss="modal"><span>Close</span></button>
                         <button type="submit" class="btn btn-primary"><span>Save changes</span></button>
                     </div>
                 </form>
             </div>
         </div>
     </div>
@endsection

