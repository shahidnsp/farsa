@extends('frontend.layout.app')

@section('title', 'Checkout | FARSA Restaurants')

@section('content')
    <!-- Page Title -->
            <div class="page-title bg-dark dark">
                <!-- BG Image -->
                <div class="bg-image bg-parallax"><img src="assets/img/photos/bg-croissant.jpg" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 push-lg-4">
                            <h1 class="mb-0">Checkout</h1>
                            <h4 class="text-muted mb-0">Some informations about our restaurant</h4>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Section -->
            <section class="section bg-light">
                <?php $order=\App\Order::where('users_id', \Illuminate\Support\Facades\Auth::id())->where('paymentstatus', 0)->with('orderitems.product.product')->get()->last() ?>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 push-xl-8 col-lg-5 push-lg-7">
                            <div class="shadow bg-white stick-to-content mb-4">
                                <div class="bg-dark dark p-4"><h5 class="mb-0">You order</h5></div>
                                <table class="table-cart">
                                    @if($order!=null)
                                    @foreach($order->orderitems as $item)
                                    <tr>
                                        <td class="title">
                                            <span class="name"><a href="#productModal" ng-click="showExistingProduct({{$item}});" data-toggle="modal">{{$item->product->product->name}}</a></span>
                                            <span class="caption text-muted">{{$item->product->name}}</span>
                                        </td>
                                        <td class="price">{{$item->qty}} X &#8377;{{$item->product->amount}}</td>
                                        <td class="actions">
                                            <a href="#productModal" data-toggle="modal" ng-click="showExistingProduct({{$item}});" class="action-icon"><i class="ti ti-pencil"></i></a>
                                            <form id="mycheckoutform" method="post" action="{{route('removeMyOrder')}}" style="cursor: pointer;">
                                                  {{ csrf_field() }}
                                                  <input type="hidden" name="products_id" value="{{$item->products_id}}"/>
                                                  <input type="hidden" name="orders_id" value="{{$item->orders_id}}"/>
                                                  <a href="#" class="action-icon" onclick="document.getElementById('mycheckoutform').submit();"><i class="ti ti-close"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                   @endforeach
                                    {{--<tr>
                                        <td class="title">
                                            <span class="name">Weekend 20% OFF</span>
                                        </td>
                                        <td class="price text-success">- &#8377;8.22</td>
                                        <td class="actions"></td>
                                    </tr>--}}
                                    @endif
                                </table>
                                <div class="cart-summary">
                                    <div class="row">
                                        <div class="col-7 text-right text-muted">Order Total:</div>
                                        <div class="col-5"><strong> &#8377;@if($order!=null){{$order->total}}@else 0 @endif</strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-7 text-right text-muted">Discount:</div>
                                        <div class="col-5"><strong>-&#8377;@if($order!=null){{($order->total*$order->discount)/100}}@else 0 @endif</strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-7 text-right text-muted">GST:</div>
                                        <div class="col-5 text-info"><strong> &#8377;@if($order!=null){{$order->gst}}@else 0 @endif</strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-7 text-right text-muted">Devliery:</div>
                                        <div class="col-5"><strong> &#8377;@if($order!=null){{$order->deliverycharge}}@else 0 @endif</strong></div>
                                    </div>
                                    <hr class="hr-sm">
                                    <div class="row text-md">
                                        <div class="col-7 text-right text-muted">Total:</div>
                                        <div class="col-5 text-danger"><strong> &#8377;@if($order!=null){{$order->grandtotal}}@else 0 @endif</strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $address=\App\Address::where('users_id',\Illuminate\Support\Facades\Auth::id())->get()->last(); ?>
                        <div class="col-xl-8 pull-xl-4 col-lg-7 pull-lg-5">
                            <form action="{{route('makeMyPayment')}}" method="post">
                                {{ csrf_field() }}
                                <input name="orders_id" type="hidden" value=" @if($order!=null) {{$order->id}}@endif"/>
                                <div class="bg-white p-4 p-md-5 mb-4">
                                    <h4 class="border-bottom pb-4"><i class="ti ti-user mr-3 text-primary"></i>Basic informations</h4>
                                    <div class="row mb-5">
                                        <div class="form-group col-sm-6">
                                            <label>Name:</label>
                                            <input type="text" name="name" value="@if($address!=null){{$address->name}}@endif" class="form-control" required="">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Phone number:</label>
                                            <input type="text" name="phone" value="@if($address!=null){{$address->phone}}@endif" class="form-control" required="">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Pincode:</label>
                                            <input type="text" name="pincode" value="@if($address!=null){{$address->pincode}}@endif" class="form-control" required="">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Street and Locality:</label>
                                            <input type="text" name="street" value="@if($address!=null){{$address->street}}@endif" class="form-control" required="">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Landmark:</label>
                                            <input type="text" name="landmark" value="@if($address!=null){{$address->landmark}}@endif" class="form-control" required="">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Alternate Phone(Optional):</label>
                                            <input type="text" name="phone2" value="@if($address!=null){{$address->phone2}}@endif" class="form-control">
                                        </div>
                                        <div class="form-group col-sm-12">
                                            <label>Address (Area and Street):</label>
                                            <textarea name="address" cols="30" rows="4" class="form-control">@if($address!=null){{$address->address}}@endif</textarea>
                                        </div>
                                    </div>
                                    <h4 class="border-bottom pb-4"><i class="ti ti-package mr-3 text-primary"></i>Delivery</h4>
                                    <div class="row mb-5">
                                        <div class="form-group col-sm-6">
                                            <label>Delivery time:</label>
                                            <div class="select-container">
                                                <select name="deliverytime" class="form-control">
                                                    <option value="As Fast as Possible" selected>As Fast as Possible</option>
                                                    <option value="In One Hour">In One Hour</option>
                                                    <option value="In Two Hours">In Two Hours</option>
                                                    <option value="In Three Hours">In Three Hours</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="border-bottom pb-4"><i class="ti ti-wallet mr-3 text-primary"></i>Payment</h4>
                                    <div class="row text-lg">
                                        <div class="col-md-4 col-sm-6 form-group">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" name="paymenttype" value="COD" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Cash on Delivery</span>
                                            </label>
                                        </div>
                                        <div class="col-md-4 col-sm-6 form-group">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" name="paymenttype" value="Online" class="custom-control-input" checked>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Online Payment</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-lg"><span>Order now!</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>
@endsection

