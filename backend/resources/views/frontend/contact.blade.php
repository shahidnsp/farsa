@extends('frontend.layout.app')

@section('title', 'Contact Us | FARSA Restaurants')

@section('content')
  <div class="page-title bg-light">
      <div class="container">
          <div class="row">
              <div class="col-lg-8 push-lg-4">
                  <h1 class="mb-0">Contact Us</h1>
                  <h4 class="mb-0">Don't hesitate to write to us</h4>
              </div>
          </div>
      </div>
  </div>

  <!-- Section -->
  <section class="section">

      <div class="container">
          <div class="row align-items-center">
              <div class="col-lg-4 push-lg-1 col-md-6">
                  <img src="frontend/assets/img/logo-contact.jpg" alt="" class="mb-2" width="130">
                  <h4 class="mb-0">Farsa Restaurant</h4>
                  <span class="text-muted">Rajiv Gandhi bypass, Manjeri</span><br />
                  <span class="text-muted">Near Sree Suma Auditorioum</span>
                  <hr class="hr-md">
                  <div class="row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                          <h6 class="mb-1 text-muted">Phone:</h6>
                          +91 9749 888 333 (444)
                      </div>
                      <div class="col-sm-6">
                          <h6 class="mb-1 text-muted">E-mail:</h6>
                          <a href="info@farsagroup.com">info@farsagroup.com</a>
                      </div>
                  </div>
                  <hr class="hr-md">
                  <h6 class="mb-3 text-muted">Follow Us!</h6>
                  <a href="https://www.facebook.com/farsagroup/" target="_blank" class="icon icon-social icon-circle icon-sm icon-facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="icon icon-social icon-circle icon-sm icon-youtube"><i class="fa fa-youtube"></i></a>
                  <a href="#" class="icon icon-social icon-circle icon-sm icon-instagram"><i class="fa fa-instagram"></i></a>
              </div>
              <div class="col-lg-5 push-lg-2 col-md-6">
                <div class="map-bo">
                  
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1163.8958484893624!2d76.11432789143798!3d11.123550826726108!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba6366b95555555%3A0x41a08fadffeedf13!2sFarsa+Restaurant!5e0!3m2!1sen!2sin!4v1544990375749" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>
          </div>
      </div>

  </section>
  @include('frontend.partials.googleapis')
@endsection

