@extends('frontend.layout.app')

@section('title', 'About Us | FARSA Restaurants')

@section('content')
     <!-- Page Title -->
     <div class="page-title bg-light">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 push-lg-4">
                     <h1 class="mb-0">Our Facilities</h1>
                     <h4 class="mb-0">Delivering auspicious treat to taste buds for more than 75 years</h4>
                 </div>
             </div>
         </div>
     </div>

     <!-- Section -->
     <section class="section section-double">
         <div class="row no-gutters">
             <div class="content col-xl-4 col-md-5">
                 <h2>COOL</h2>
                 <p class="lead text-muted">We maintain the highest possible standards of hygiene and cleanliness. Our well-trained and courteous staff members make your dining experience quick and pleasant.</p>
                 <a href="/contact" class="btn btn-outline-primary btn-lg"><span>Get a quote</span></a>
             </div>
             <div class="image col-xl-8 col-md-7">
                 <div class="bg-image"><img src="frontend/assets/img/service-weddings.jpg" alt=""></div>
             </div>
         </div>
     </section>

     <!-- Section -->
     <section class="section section-double">
         <div class="row no-gutters">
             <div class="content col-xl-4 col-md-5 push-xl-8 push-md-7">
                 <h2>DINEIN</h2>
                 <p class="lead text-muted">DineIn restaurant’s physical infrastructure has a direct effect on customers’ brand experience..</p>
                 <a href="/contact" class="btn btn-outline-primary btn-lg"><span>Get a quote</span></a>
             </div>
             <div class="image col-xl-8 col-md-7 pull-xl-4 pull-md-5">
                 <div class="bg-image"><img src="frontend/assets/img/service-engagement.jpg" alt=""></div>
             </div>
         </div>
     </section>

     <!-- Section -->
     <section class="section section-double">
         <div class="row no-gutters">
             <div class="content col-xl-4 col-md-5">
                 <h2>SALKARA</h2>
                 <p class="lead text-muted">We have a large spacious room available for conferences/training/meetings, suitable for approximately 40 people and fitted with food.</p>
                 <a href="/contact" class="btn btn-outline-primary btn-lg"><span>Get a quote</span></a>
             </div>
             <div class="image col-xl-8 col-md-7">
                 <div class="bg-image"><img src="frontend/assets/img/service-birthday.jpg" alt=""></div>
             </div>
         </div>
     </section>

     <!-- Section -->
     <section class="section section-double">
         <div class="row no-gutters">
             <div class="content col-xl-4 col-md-5 push-xl-8 push-md-7">
                 <h2>HOME</h2>
                 <p class="lead text-muted">We have a wonderful outdoor sitting area, Overlooking the surrounding lush fields, equipped with a table and surrounding benches.</p>
                 <a href="/contact" class="btn btn-outline-primary btn-lg"><span>Get a quote</span></a>
             </div>
             <div class="image col-xl-8 col-md-7 pull-xl-4 pull-md-5">
                 <div class="bg-image"><img src="frontend/assets/img/service-engagement.jpg" alt=""></div>
             </div>
         </div>
     </section>


     <!-- Section -->
     <section class="section section-lg dark bg-dark">

         <div class="bg-image bg-parallax"><img src="frontend/assets/img/bg-croissant.jpg" alt=""></div>

         <div class="container text-center">
             <div class="col-lg-8 push-lg-2">
                 <h2 class="mb-3">Would you like to visit Us?</h2>
                 <h5 class="text-muted">Book a table even right now or make an online order!</h5>
                 <a href="/" class="btn btn-primary"><span>Order Online</span></a>
                 <a href="/contact" class="btn btn-outline-primary"><span>Book a table</span></a>
             </div>
         </div>

     </section>
@endsection

