@extends('frontend.layout.app')

@section('title', 'Home | FARSA Restaurants')

@section('content')
            <section class="section section-main section-main-2 bg-dark dark">
                <div id="section-main-2-slider" class="section-slider inner-controls">
                    <!-- Slide -->
                    <!--<div class="slide">
                        <div class="bg-image zooming"><img src="frontend/assets/img/slider-burger_dark.jpg" alt=""></div>
                        <div class="container v-center">
                            <h1 class="display-2 mb-2">Get 10% off coupon</h1>
                            <h4 class="text-muted mb-5">and use it with your next order!</h4>
                            <a href="page-offers.html" class="btn btn-outline-primary btn-lg"><span>Get it now!</span></a>
                        </div>
                    </div>-->
                    <!-- Slide -->
                    <!--<div class="slide">
                        <div class="bg-image zooming"><img src="frontend/assets/img/slider-dessert_dark.jpg" alt=""></div>
                        <div class="container v-center">
                            <h1 class="display-2 mb-2">Delicious Desserts</h1>
                            <h4 class="text-muted mb-5">Order it online even now!</h4>
                            <a href="menu-list-collapse.html" class="btn btn-outline-primary btn-lg"><span>Order now!</span></a>
                        </div>
                    </div>-->
                    <!-- Slide -->
                    <?php $sliders=\App\Slider::with('product.prices')->get();  ?>

                    @foreach($sliders as $slider)
                        @if($slider->product!=null)
                            <div class="slide">
                                {{--<div class="bg-image zooming"><img src="{{url('images/'.$slider->photo)}}" alt=""></div>--}}
                                <?php
                                       $img = Image::cache(function($image) use ($slider) {
                                              $image->make(storage_path().'/app/'.$slider->photo)->resize(1860, 852);
                                       });
                                ?>
                                <div class="bg-image zooming"><img src="{{'data:image/jpg'  . ';base64,' . base64_encode($img)}}" alt=""></div>
                                <div class="container v-center">
                                    <h4 class="text-muted">{{$slider->name}}!</h4>
                                    <h1 class="display-2">{{$slider->product->name}}</h1>
                                    <div class="btn-group">
                                        <a href="#productModal" ng-click="showOfferProduct({{$slider->product}});" data-toggle="modal" class="btn btn-outline-primary btn-lg"><span>Add to cart</span></a>
                                        <span class="price price-lg">from &#8377; @if(count($slider->product->prices)!=0) {{$slider->product->prices[0]->amount}} @else 0 @endif</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </section>

            <!-- Section - About -->
            <section class="section section-bg-edge">
                <div class="image right col-md-6 push-md-6">
                    <div class="bg-image"><img src="frontend/assets/img/bg-home.jpg" alt=""></div>
                </div>
                <div class="container">
                    <div class="col-lg-5 col-md-9">
                        <div class="rate mb-5 rate-lg"><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star active"></i><i class="fa fa-star"></i></div>
                        <h1>The best food in Malabar!</h1>
                        <p class="lead text-muted mb-5">
                            Delivering auspicious treat to taste buds for more than 75 years, Farsa Restaurant is becoming a whole new quality eat-hub. Farsa starting small in the initial period, understanding the regional magic helped to continually amaze the customers with delicious foods...
                        </p>
                        <div class="blockquotes">

                            <?php $testimonials=\App\Testimonial::orderBy('id', 'desc')->where('active',1)->take(2)->get(); ?>
                            @foreach($testimonials as $index=>$testimonial)
                                <!-- Blockquote -->
                                <blockquote class="blockquote animated @if($index==0) light @endif" data-animation="fadeInLeft">
                                    <div class="blockquote-content @if($index==1) dark @endif">
                                        <div class="rate rate-sm mb-3">
                                            @for($i=1;$i<$testimonial->rating;$i++)
                                                <i class="fa fa-star active"></i>
                                            @endfor
                                        </div>
                                        <p>{{$testimonial->description}}</p>
                                    </div>
                                    <footer>
                                        <img src="{{url('images/'.$testimonial->photo)}}" alt="">
                                        <span class="name">{{$testimonial->name}}<span class="text-muted">, {{$testimonial->designation}}</span></span>
                                    </footer>
                                </blockquote>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

            <!-- Section - Steps -->
            <section class="section section-extended left dark">
                <div class="container bg-dark">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Step -->
                            <div class="feature feature-1 mb-md-0">
                                <div class="feature-icon icon icon-primary"><i class="ti ti-shopping-cart"></i></div>
                                <div class="feature-content">
                                    <h4 class="mb-2"><a href="menu-list-collapse.html">Pick a dish</a></h4>
                                    <p class="text-muted mb-0">Enjoy the comfort of exquisitely prepared meals, without the hassle of cooking yourself.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- Step -->
                            <div class="feature feature-1 mb-md-0">
                                <div class="feature-icon icon icon-primary"><i class="ti ti-wallet"></i></div>
                                <div class="feature-content">
                                    <h4 class="mb-2">Make a payment</h4>
                                    <p class="text-muted mb-0">Make Secure and Fast payment steps to create impressive dinners worth sharing.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- Step -->
                            <div class="feature feature-1 mb-md-0">
                                <div class="feature-icon icon icon-primary"><i class="ti ti-package"></i></div>
                                <div class="feature-content">
                                    <h4 class="mb-2">Receive your food!</h4>
                                    <p class="text-muted mb-3">Pick up your prepared meals from delivery points.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Section - Menu -->
            <section class="section pb-0">
                <div class="container">
                    <h1 class="text-center mb-6">Our menu</h1>
                </div>
                <div class="menu-sample-carousel carousel inner-controls"
                    data-slick='{
                            "dots": true,
                            "slidesToShow": 3,
                            "slidesToScroll": 1,
                            "infinite": true,
                            "responsive": [
                                {
                                    "breakpoint": 991,
                                    "settings": {
                                        "slidesToShow": 2,
                                        "slidesToScroll": 1
                                    }
                                },
                                {
                                    "breakpoint": 690,
                                    "settings": {
                                        "slidesToShow": 1,
                                        "slidesToScroll": 1
                                    }
                                }
                            ]
                        }'>

                    <?php $menues=\App\Menu::where('active',1)->get();?>

                    <!-- Menu Sample -->
                  @foreach($menues as $menu)
                        <div class="menu-sample">
                            <a href="/menu#{{$menu->id}}">
                                {{--<img src="{{url('images/'.$menu->photo)}}" alt="" class="image">--}}
                                 <?php
                                       $img = Image::cache(function($image) use ($menu) {
                                              $image->make(storage_path().'/app/'.$menu->photo)->resize(620, 542);
                                       });
                                ?>
                                <img src="{{'data:image/jpg'  . ';base64,' . base64_encode($img)}}" alt="" class="image">
                                <h3 class="title">{{ str_limit($menu->name, $limit = 8, $end = '..') }}</h3>
                            </a>
                        </div>
                    @endforeach

                </div>
            </section>

            <!-- Section - Offers -->
            <section class="section">
                <div class="container">
                    <h1 class="text-center mb-6">Special offers</h1>
                    <?php $offers=\App\Offer::all(); ?>
                    <div class="carousel" data-slick='{"dots": true}'>
                        <!-- Special Offer -->
                        @foreach($offers as $offer)
                        <div class="special-offer">
                            <img src="{{url('images/'.$offer->photo)}}" alt="" class="special-offer-image">
                            <div class="special-offer-content">
                                <h2 class="mb-2">{{$offer->name}}</h2>
                                <p>{{$offer->description}}</p>
                               {{-- <h5 class="text-muted mb-3">Get free burger from orders higher that &#8377;40!</h5>
                                <ul class="list-check text-lg mb-0">
                                    <li>Only on Tuesdays</li>
                                    <li class="false">Order higher that &#8377;40</li>
                                    <li>Unless one burger ordered</li>
                                </ul>--}}
                                <div class="btn-group float-right">
                                    <a href="/menu" data-toggle="modal" class="btn btn-outline-primary btn-lg"><span>View</span></a>
                                    <span class="price price-lg">from &#8377;{{$offer->amount}}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>

            <!-- Section -->
            <section class="section section-lg dark bg-dark">
                <!-- BG Image -->
                <div class="bg-image bg-parallax"><img src="frontend/assets/img/bg-croissant.jpg" alt=""></div>
                <div class="container text-center">
                    <div class="col-lg-8 push-lg-2">
                        <h2 class="mb-3">Check our promo video!</h2>
                        <h5 class="text-muted">Book a table even right now or make an online order!</h5>
                        <button class="btn-play" data-toggle="video-modal" data-target="#modalVideo" data-video="https://www.youtube.com/embed/bAuWrPzmGoM"></button>
                    </div>
                </div>
            </section>
@endsection

