<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="FARSA food Online Restaurants" />
    <meta property="og:site_name" content="FARSA food Online Restaurants" />
    <meta property="og:url" content="http://farsagroup.com/" />
    <meta property="og:description" content="FARSA Restaurants Online Order from the best restaurants in Manjeri | Order food online with menus, reviews and Pay Online" />
    <meta property="og:type" content="website" />
    <title>@yield('title')</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="FARSA Restaurants Online Order from the best restaurants in Manjeri | Order food online with menus, reviews and Pay Online">
    <meta name="keywords" content="FARSA Restaurants,FARSA Manjeri, Broast Manjeri, Food in Manjeri, order food online, food online Manjeri, Restaurants in Manjeri" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{asset('frontend/assets/img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('frontend/assets/img/favicon_60x60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('frontend/assets/img/favicon_76x76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('frontend/assets/img/favicon_120x120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('frontend/assets/img/favicon_152x152.png')}}">
    <!-- CSS Plugins -->
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/slick-carousel/slick/slick.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/animate.css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/animsition/dist/css/animsition.min.css')}}" />
    <!-- CSS Icons -->
    <link rel="stylesheet" href="{{asset('frontend/assets/css/themify-icons.css')}}" />
    <link rel="stylesheet" href="{{asset('frontend/assets/plugins/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Theme -->
    <link id="theme" rel="stylesheet" href="{{asset('frontend/assets/css/themes/theme-beige.min.css')}}" />

</head>
<body ng-controller="IndexController">

<!-- Body Wrapper -->
<div id="body-wrapper" class="animsition">
    <!-- Header -->
    <header id="header" class="light">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Logo -->
                    <div class="module module-logo light">
                        <a href="/">
                            <img src="{{asset('frontend/assets/img/logo.jpg')}}" alt="" width="88">
                        </a>
                    </div>
                </div>
                <div class="col-md-7">
                    <!-- Navigation -->
                    <nav class="module module-navigation left mr-4">
                        <ul id="nav-main" class="nav nav-main">
                            <li><a href="/">Home</a></li>
                            <li class="has-dropdown">
                                <a href="#">About Us</a>
                                <div class="dropdown-container">
                                    <ul class="dropdown-mega">
                                        <li><a href="/about-us">About Us</a></li>
                                        <li><a href="/facilities">Our Facilities</a></li>
                                        <li><a href="/reviews">Table Talk</a></li>
                                       {{-- <li><a href="/book-table">Book A Table</a></li>--}}
                                        <li><a href="/tandc">Terms & Conditions</a></li>
                                    </ul>
                                    <div class="dropdown-image">
                                        <img src="frontend/assets/img/dropdown-about.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a href="/menu">Our Menu</a></li>
                            <li><a href="/offers">Offers</a></li>
                           {{-- <li><a href="/book-table">Book a table</a></li>--}}
                            <li><a href="/contact">Contact</a></li>
                             @guest
                                <li><a href="/login">Login</a></li>
                             @else
                            <li class="has-dropdown sham">
                                <a href="#"><i class="ti ti-user"></i>Hi, {{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                                <div class="dropdown-container">
                                    <ul>
                                        <li><a class="dropdown-item hvr" href="/account">Account <i class="ti ti-user dricon"></i></a></li>
                                        <li><a class="dropdown-item hvr" href="/myorders">My Orders <i class="ti ti-truck dricon"></i></a></li>
                                       <!--  <li><a class="dropdown-item hvr" href="settings.html">Settings <i class="ti ti-settings dricon"></i></a></li> -->
                                        <li>
                                            <!-- <a class="dropdown-item hvr" href="logout.html">LogOut <i class="ti ti-lock dricon"></i></a> -->
                                             <form id="myform" method="post" action="{{url('logout')}}" style="cursor: pointer;">
                                                  {{ csrf_field() }}
                                                  <a class="dropdown-item hvr" onclick="document.getElementById('myform').submit();"><i class="ti ti-lock dricon"></i> LogOut</a>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </nav>
                    <div class="module left">
                        <a href="/menu" class="btn btn-outline-secondary"><span>Order</span></a>
                    </div>
                </div>

                <div class="col-md-2">
                    <!-- <a href="#" class="module module-cart right" data-toggle="panel-cart">
                        <span class="cart-icon">
                            <i class="ti ti-shopping-cart"></i>
                            <span class="notification">2</span>
                        </span>
                        <span class="cart-value">&#8377;32.98</span>
                    </a> -->
                    <?php $order=\App\Order::where('users_id', \Illuminate\Support\Facades\Auth::id())->where('paymentstatus', 0)->with('orderitems.product.product')->get()->last() ?>
                    <a href="#" class="module module-cart right" data-toggle="panel-cart">
                        <span class="cart-icon">
                            <i class="ti ti-shopping-cart"></i>
                            <span class="notification">@if($order!=null){{$order->orderitems->count()}}@else 0 @endif</span>
                        </span>
                        <span class="cart-value">&#8377;@if($order!=null){{$order->grandtotal}}@else 0.00 @endif</span>
                    </a>
                </div>


            </div>
        </div>
    </header>
    <!-- Header / End -->

    <!-- Header -->
    <header id="header-mobile" class="light">
        <div class="module module-nav-toggle">
            <a href="#" id="nav-toggle" data-toggle="panel-mobile"><span></span><span></span><span></span><span></span></a>
        </div>
        <div class="module module-logo">
            <a href="/">
                <img src="{{asset('frontend/assets/img/logo-horizontal.jpg')}}" alt="">
            </a>
        </div>
        @if(\Illuminate\Support\Facades\Auth::check())
        <a href="#" class="module module-cart" data-toggle="panel-cart">
            <i class="ti ti-shopping-cart"></i>
            <span class="notification">@if($order!=null){{$order->orderitems->count()}}@else 0 @endif</span>
        </a>
        @endif
    </header>
    <!-- Header / End -->

    <!-- Content -->
    <div id="content">
        <!-- Section - Main -->
             @yield('content')
        <!-- Section- Main End-->

        <!-- Footer -->
        <footer id="footer" class="bg-dark dark">
            <div class="container">
                <!-- Footer 2nd Row -->
                <div class="footer-second-row row align-items-center">
                    <div class="col-lg-4 text-center text-md-left">
                        <span class="text-sm">FARSA FOOD &copy;<span id="year"></span>.<br>Designed and maintained by <a href="http://cloudbery.com/" target="_blank"><img src="frontend/assets/img/cloudbery.png"></a></span>
                    </div>
                    <div class="col-lg-4 text-center">
                        <a href="/"><img src="{{asset('frontend/assets/img/logo-footer.png')}}" alt="" width="88" class="mt-5 mb-5"></a>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center text-md-right">
                        <a href="/tandc">Terms & Conditions</a>
                        <a href="https://www.facebook.com/farsagroup/" target="_blanlk" class="icon icon-social icon-circle icon-sm icon-facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="icon icon-social icon-circle icon-sm icon-youtube"><i class="fa fa-youtube"></i></a>
                        <a href="#" class="icon icon-social icon-circle icon-sm icon-instagram"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <!-- Back To Top -->
            <a href="#" id="back-to-top"><i class="ti ti-angle-up"></i></a>
        </footer>
        <!-- Footer / End -->
    </div>
    <!-- Content / End -->

   @if(\Illuminate\Support\Facades\Auth::check())
    <!-- Panel Cart -->
    <div id="panel-cart">
        <div class="panel-cart-container">
            <div class="panel-cart-title">
                <h5 class="title">Your Cart</h5>
                <button class="close" data-toggle="panel-cart"><i class="ti ti-close"></i></button>
            </div>
            <div class="panel-cart-content">



                <table class="table-cart">
                    @if($order!=null)
                        @foreach($order->orderitems as $item)
                        <tr>
                            <td class="title">
                                <span class="name"><a href="#productModal" ng-click="showExistingProduct({{$item}});" data-toggle="modal">{{$item->product->product->name}}</a></span>
                                <span class="caption text-muted">{{$item->product->name}}</span>
                            </td>
                            <td class="price">{{$item->qty}} X &#8377;{{$item->product->amount}}</td>
                            <td class="actions">
                                <a href="#productModal" ng-click="showExistingProduct({{$item}});" data-toggle="modal" class="action-icon"><i class="ti ti-pencil"></i></a>
                                <form id="mycartform" method="post" action="{{route('removeMyOrder')}}" style="cursor: pointer;">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="products_id" value="{{$item->products_id}}"/>
                                      <input type="hidden" name="orders_id" value="{{$item->orders_id}}"/>
                                      <a href="#" class="action-icon" onclick="document.getElementById('mycartform').submit();"><i class="ti ti-trash"></i></a>
                                </form>
                            </td>
                        </tr>
                        @endforeach


                    @endif
                </table>
                <div class="cart-summary">
                    <div class="row">
                        <div class="col-7 text-right text-muted">Order Total:</div>
                        <div class="col-5"><strong>&#8377;@if($order!=null){{$order->total}}@else 0 @endif</strong></div>
                    </div>
                   {{-- <div class="row">
                        <div class="col-7 text-right text-muted">Discount:</div>
                        <div class="col-5 text-info"><strong>&#8377;@if($order!=null){{($order->total*$order->discount)/100}}@else 0 @endif</strong></div>
                    </div>--}}
                    <div class="row">
                        <div class="col-7 text-right text-muted">GST:</div>
                        <div class="col-5 text-info"><strong>&#8377;@if($order!=null){{$order->gst}}@else 0 @endif</strong></div>
                    </div>
                    <div class="row">
                        <div class="col-7 text-right text-muted">Devliery:</div>
                        <div class="col-5"><strong>&#8377;@if($order!=null){{$order->deliverycharge}}@else 0 @endif</strong></div>
                    </div>
                    <hr class="hr-sm">
                     <?php
                        $day=\Illuminate\Support\Carbon::now()->format('l');
                        $discountper=0;
                        $offerper=\App\WeekendOffer::where('day',$day)->where('active',1)->get()->first();
                        if($offerper!=null){
                            $discountper=$offerper->amount;
                        }
                     ?>
                     @if($discountper!=0)
                        <tr>
                            <td class="title">
                                <span class="name text-danger">{{$day}} {{round($discountper)}}% OFF</span>
                            </td>
                            <td class="price text-success">-&#8377;@if($order!=null){{($order->total*$order->discount)/100}}@else 0 @endif</td>
                            <td class="actions"></td>
                        </tr>
                     @endif
                    <hr class="hr-sm">
                    <div class="row text-lg">
                        <div class="col-7 text-right text-muted">Total:</div>
                        <div class="col-5 text-danger"><strong>&#8377;@if($order!=null){{$order->grandtotal}}@else 0 @endif</strong></div>
                    </div>



                </div>
            </div>
        </div>
        <a href="/checkout" class="panel-cart-action btn btn-secondary btn-block btn-lg"><span>Go to checkout</span></a>
    </div>
    @endif
    <!-- Panel Mobile -->
    <nav id="panel-mobile">
        <div class="module module-logo bg-dark dark">
            <a href="#">
                <img src="{{asset('frontend/assets/img/logo-light.svg')}}" alt="" width="88">
            </a>
            <button class="close" data-toggle="panel-mobile"><i class="ti ti-close"></i></button>
        </div>
        <nav class="module module-navigation"></nav>
        <div class="module module-social">
            <h6 class="text-sm mb-3">Follow Us!</h6>
            <a href="https://www.facebook.com/farsagroup/" target="_blanlk" class="icon icon-social icon-circle icon-sm icon-facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="icon icon-social icon-circle icon-sm icon-youtube"><i class="fa fa-youtube"></i></a>
            <a href="#" class="icon icon-social icon-circle icon-sm icon-instagram"><i class="fa fa-instagram"></i></a>
        </div>
    </nav>

    <!-- Body Overlay -->
    <div id="body-overlay"></div>

</div>

<!-- Modal / Product -->
<div class="modal fade" id="productModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="frontend/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Specify your dish</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="row align-items-center">
                    <div class="col-9">
                        <h6 class="mb-0">@{{myproduct.name}}</h6>
                        <span class="text-muted">@{{myproduct.description | cut:true:50:' ...'}}</span>
                    </div>
                    <div class="col-3 text-lg text-right">&#8377;@{{myproduct.prices[0].amount}}</div>
                </div>
            </div>
             <form action="{{route('takeMyOrder')}}" method="post">
                 {{ csrf_field() }}
                <div class="modal-body panel-details-container">
                    <!-- Panel Details / Size -->
                    <div class="panel-details">
                        <h5 class="panel-details-title">
                            <label class="custom-control custom-radio">
                                <input name="radio_title_size" type="radio" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                            <a href="#panelDetailsSize" data-toggle="collapse">Size</a>
                        </h5>
                        <div id="panelDetailsSize" class="collapse show">
                            <div class="panel-details-content">
                                <div ng-show="new" ng-repeat="price in myproduct.prices" class="form-group">
                                    <label class="custom-control custom-radio">
                                        <input name="products_id" type="radio" value="@{{price.id}}" class="custom-control-input" checked>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">@{{price.name}} (&#8377;@{{price.amount}})</span>
                                    </label>
                                </div>

                                <div ng-hide="new" ng-repeat="price in myproduct.prices" class="form-group">
                                    <label class="custom-control custom-radio">
                                        <input name="products_id" type="radio" value="@{{price.id}}" class="custom-control-input" ng-checked="price.id==myproduct_id">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">@{{price.name}} (&#8377;@{{price.amount}})</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-details">
                        <h5 class="panel-details-title">
                            <label class="custom-control custom-radio">
                                <input name="radio_title_size" type="radio" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                            <a href="#panelDetailsQty" data-toggle="collapse">Quantity</a>
                        </h5>
                        <div id="panelDetailsQty" class="collapse show">
                            <div class="panel-details-content">
                                <div  class="form-group">
                                    <input name="qty" type="number" min="1" ng-model="qty" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Panel Details / Other -->
                    <div class="panel-details">
                        <h5 class="panel-details-title">
                            <label class="custom-control custom-radio">
                                <input name="radio_title_other" type="radio" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                            <a href="#panelDetailsOther" data-toggle="collapse">Other</a>
                        </h5>
                        <div id="panelDetailsOther" class="collapse">
                            <textarea name="description" cols="30" rows="4" class="form-control" placeholder="Put this any other informations..."></textarea>
                        </div>
                    </div>
                     <span style="text-align: center;" class="text-danger">Delivery Only around 10 KM from Manjeri</span>
                </div>

                <button type="submit" class="modal-btn btn btn-secondary btn-block btn-lg"><span>Add to Cart</span></button>
            </form>
        </div>
    </div>
</div>

<!-- Video Modal / Demo -->
<div class="modal modal-video fade" id="modalVideo" role="dialog">
    <button class="close" data-dismiss="modal"><i class="ti-close"></i></button>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <iframe height="500"></iframe>
        </div>
    </div>
</div>

<!-- JS Plugins -->
<script src="{{asset('frontend/assets/plugins/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/slick-carousel/slick/slick.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/jquery.appear/jquery.appear.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/jquery.scrollto/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/jquery.localscroll/jquery.localScroll.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/twitter-fetcher/js/twitterFetcher_min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/skrollr/dist/skrollr.min.js')}}"></script>
<script src="{{asset('frontend/assets/plugins/animsition/dist/js/animsition.min.js')}}"></script>
<!-- JS Core -->
<script src="{{asset('frontend/assets/js/core.js')}}"></script>
<!-- JS Stylewsitcher -->
<script src="{{asset('frontend/styleswitcher/styleswitcher.js')}}"></script>
<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script>
    var app = angular.
        module('myApp', []);

        app.filter('cut', function () {
           return function (value, wordwise, max, tail) {
               if (!value) return '';

               max = parseInt(max, 10);
               if (!max) return value;
               if (value.length <= max) return value;

               value = value.substr(0, max);
               if (wordwise) {
                   var lastspace = value.lastIndexOf(' ');
                   if (lastspace !== -1) {
                     //Also remove . and , so its gives a cleaner result.
                     if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                       lastspace = lastspace - 1;
                     }
                     value = value.substr(0, lastspace);
                   }
               }

               return value + (tail || ' …');
           };
       });

        app.controller('IndexController', function($scope,$http){
            $scope.myproduct={};

            $scope.qty=1;
            $scope.new=true;

            $scope.showMyProduct=function(item){
                $scope.new=true;
                $scope.myproduct=item;
            };

            $scope.showExistingProduct=function(item){
                $scope.new=false;
                var products_id=item.product.products_id;

                $scope.myproduct_id=item.product.id;

                $scope.qty=item.qty;
                $http({
                      method: 'GET',
                      url: '/web/getMyProduct/'+products_id
                   }).then(function (success){
                        console.log(success);
                        $scope.myproduct=success.data;
                       // $scope.myproduct=success.data;
                   },function (error){

                   });
            };

            $scope.showOfferProduct=function(item){
                $scope.new=true;
                var products_id=item.id;
                $http({
                      method: 'GET',
                      url: '/web/getMyProduct/'+products_id
                   }).then(function (success){
                        console.log(success);
                        $scope.myproduct=success.data;
                       // $scope.myproduct=success.data;
                   },function (error){

                   });
            };
        });
</script>
</body>
</html>
