@extends('frontend.layout.app')

@section('title', 'Our Menu | FARSA Restaurants')

@section('content')
     <div class="page-title bg-light">
                 <div class="container">
                     <div class="row">
                         <div class="col-lg-8 push-lg-4">
                             <h1 class="mb-0">Our Menu</h1>
                             <h4 class="text-muted mb-0">Delivering auspicious treat to taste buds for more than 75 years...</h4>
                         </div>
                     </div>
                 </div>
             </div>

             <!-- Page Content -->
             <div class="page-content">
                 <div class="container">
                     <div class="row no-gutters">
                         <div class="col-md-3">
                             <!-- Menu Navigation -->
                             <?php
                                $menues = \Illuminate\Support\Facades\Cache::remember('menues', 2*60, function() {
                                                 return \App\Menu::with('products.prices')->where('active',1)->get();
                                             });
                             ?>
                             <nav id="menu-navigation" class="stick-to-content" data-local-scroll>
                                 <ul class="nav nav-menu bg-dark dark">
                                    @foreach($menues as $menu)
                                     <li><a href="#{{$menu->id}}">{{$menu->name}}</a></li>
                                    @endforeach
                                 </ul>
                             </nav>
                         </div>
                         <div class="col-md-9">
                             @foreach($menues as $menu)
                             <!-- Menu Category / Burgers -->
                             <div id="{{$menu->id}}" class="menu-category">
                                 <div class="menu-category-title">
                                     <div class="bg-image"><img src="frontend/assets/img/photos/menu-title-burgers.jpg" alt=""></div>
                                     <h2 class="title">{{$menu->name}}</h2>
                                 </div>
                                 <div class="menu-category-content padded">
                                     <div class="row gutters-sm">
                                         @foreach($menu->products as $product)
                                         <div class="col-lg-4 col-6">
                                             <!-- Menu Item -->
                                             <div class="menu-item menu-grid-item">
                                                 {{--<img class="mb-4" src="{{url('images/'.$product->photo)}}" alt="">--}}
                                                   <?php
                                                       $img = Image::cache(function($image) use ($product) {
                                                              $image->make(storage_path().'/app/'.$product->photo)->resize(282, 129);
                                                       });
                                                   ?>
                                                   <img class="mb-4" src="{{'data:image/png'  . ';base64,' . base64_encode($img)}}" alt="">

                                                 <h6 class="mb-0">{{$product->name}}</h6>
                                                 <span class="text-muted text-sm">{{ str_limit($product->description, $limit = 30, $end = '...') }}</span>
                                                 <div class="row align-items-center mt-4">
                                                     <div class="col-sm-6"><span class="text-md mr-4"><span class="text-muted">From</span> @if(count($product->prices)>0) &#8377; {{$product->prices[0]->amount}} @endif </span></div>
                                                     <div class="col-sm-6 text-sm-right mt-2 mt-sm-0"><button ng-click="showMyProduct({{$product}});" class="btn btn-outline-secondary btn-sm" data-target="#productModal" data-toggle="modal"><span>Add to cart</span></button></div>
                                                 </div>
                                             </div>
                                         </div>
                                         @endforeach

                                     </div>
                                 </div>
                             </div>
                             @endforeach

                         </div>
                     </div>
                 </div>
             </div>
@endsection

