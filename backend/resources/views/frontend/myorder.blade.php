@extends('frontend.layout.app')

@section('title', 'My Order | FARSA Restaurants')

@section('content')
     <section id="main" class="container mt-7">
         <div class="row">
             <div class="col-md-9">
                <?php $orders=\App\Order::where('paymentstatus','!=',0)->where('users_id',\Illuminate\Support\Facades\Auth::id())->with('orderitems.product.product')->orderBy('id','desc')->get(); ?>

                 @foreach($orders as $order)
                 <!-- one grid -->
                 <div class="mb-5">
                     <div class="example-box">
                         <div class="example-box-title">
                             <span class="oron">Ordered On</span>{{\Illuminate\Support\Carbon::parse($order->datetime)->format('D, M d Y')}}
                             @if($order->paymentstatus==1)
                                <span class="or-success">Success</span>
                             @else
                                <span class="or-failed">Failed</span>
                             @endif
                         </div>
                         <div class="row p-20">
                             <div class="menu-category mb0 col-md-12">
                                 <div class="menu-category-content">
                                    @foreach($order->orderitems as $item)
                                     <!-- Menu Item -->
                                     <div class="menu-item menu-list-item">
                                         <div class="row align-items-center">
                                             <div class="col-sm-6 mb-2 mb-sm-0">
                                                 <h6 class="mb-0">{{$item->product->product->name}}</h6>
                                                 <span class="text-muted text-sm">{{$item->product->name}}</span>
                                             </div>
                                             <div class="col-sm-6 text-sm-right">
                                                 <span class="text-md mr-4"><span class="text-muted"><!-- from --></span>{{$item->qty}} X &#8377;{{$item->product->amount}}</span>
                                             </div>
                                         </div>
                                     </div>
                                    @endforeach
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="example-box-bottom">
                         <!--<span>Your item has been delivered</span>-->
                         <span>&nbsp;</span>
                         <span class="ttol">GST: ₹{{$order->gst}} | Delivery: ₹{{$order->deliverycharge}} | Order Total: ₹{{$order->grandtotal}} </span>
                     </div>
                 </div>
                 <!-- one grid -->
                @endforeach
             </div>
             <div class="col-md-3 hidden-sm hidden-xs">
                 <img src="frontend/assets/img/side1.jpg" alt="" class="mb-5">
             </div>
         </div>
     </section>
@endsection

