@extends('frontend.layout.app')

@section('title', 'Payment Failed | FARSA Restaurants')

@section('content')
       <!-- Section -->
         <section class="section bg-light">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-8 push-lg-4">
                         <span class="icon icon-xl icon-danger"><i class="ti ti-close"></i></span>
                         <h1 class="mb-2">Payment Failed</h1>
                         <h4 class="text-muted mb-5">Something went wrong</h4>
                         <a href="/menu" class="btn btn-outline-secondary"><span>Go back to menu</span></a>
                     </div>
                 </div>
             </div>
         </section>
@endsection

