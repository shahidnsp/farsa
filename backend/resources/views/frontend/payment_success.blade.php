@extends('frontend.layout.app')

@section('title', 'Payment Success | FARSA Restaurants')

@section('content')
       <!-- Section -->
         <section class="section bg-light">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-8 push-lg-4">
                         <span class="icon icon-xl icon-success"><i class="ti ti-check-box"></i></span>
                         <h1 class="mb-2">Thank you for your order!</h1>
                         <h4 class="text-muted mb-5">Your Order Placed Shortly.</h4>
                         <a href="/myorders" class="btn btn-outline-secondary"><span>Go back to My Order</span></a>
                     </div>
                 </div>
             </div>
         </section>
@endsection

