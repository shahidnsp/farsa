@extends('frontend.layout.app')

@section('title', 'Terms & Conditions| FARSA Restaurants')

@section('content')
     <!-- Page Title -->
     <article class="post single">
            <div class="post-image bg-parallax"><img src="frontend/assets/img/farsaview.jpg" alt=""></div>
            <div class="container container-md">
                <div class="post-content">
                    <h1 class="post-title">Our Terms and Conditions</h1>
                    <hr>
                    <p class="lead">
                        Please read the our terms and conditions ("Terms and Conditions") set out below carefully before ordering any Goods or Services from this Website. By ordering any Goods or Services from this Website, by phone, or by our mobile applications you agree to be bound by these Terms and Conditions.
                    </p>
                    <p class="lead">
                        Introducing www.farsagroup.com, We are Farsa Food, a brand of FARSA Group, unless otherwise stated.
                    </p>
                    <h4>Definitions</h4>
                    <ul>
                        <li>"Agreement" is a reference to these Terms and Conditions, the Privacy Policy, any order form and payment instructions provided to you;</li>
                        <li>"Privacy Policy" means the policy displayed on our Website which details how we collect and store your personal data;</li>
                        <li>"you", "your" and "yours" are references to you the person accessing this Website and ordering any Goods or Services from the Website or from any other channel provided by farsa;</li>
                        <li>"we", "us", "our", and "farsa" are references to the Company;</li>
                        <li>"Goods" is a reference to any goods which we may offer for sale from our Website from time to time;</li>
                        <li>"Service" or "Services" is a reference to any service which we may supply and which you may request via our Website;</li>
                        <li>"Participating Restaurant" is a third party, which has agreed to co-operate with the Company to prepare and/or deliver the Goods or Services.</li>
                        <li>"Food Delivery" is a reference to perishable goods and to any form of delivery service, which both are provided by our Participating Restaurants and for both of which our Participating Restaurants take full responsibility; and</li>
                        <li>"Website" is a reference to our Website http://farsagroup.com/ or our mobile applications on which we offer our Goods or Services.</li>
                    </ul>
                    <hr>
                    <h4>Ordering</h4>
                    <ul>
                        <li>Any contract for the supply of Food Delivery from this Website is between you and the Participating Restaurant; for the supply of Goods or Services from this Website any contact is between you and farsa. You agree to take particular care when providing us with your details and warrant that these details are accurate and complete at the time of ordering. You also warrant that the credit or debit card details that you provide are for your own credit or debit card and that you have sufficient funds to make the payment.</li>
                        <li>Food Delivery, Goods and Services purchased from this Website are intended for your use only and you warrant that any Goods purchased by you are not for resale and that you are acting as principal only and not as agent for another party when receiving the Services.</li>
                        <li>Please note that some of our Goods may be suitable for certain age ranges only. You should check that the product you are ordering is suitable for the intended recipient.</li>
                        <li>When ordering from this Website you may be required to provide an e-mail address and password. You must ensure that you keep the combination of these details secure and do not provide this information to a third party.</li>
                        <li>We will take all reasonable care, in so far as it is in our power to do so, to keep the details of your order and payment secure, but in the absence of negligence on our part we cannot be held liable for any loss you may suffer if a third party procures unauthorized access to any data you provide when accessing or ordering from the Website.</li>
                        <li>Any order that you place with us is subject to product availability, delivery capacity and acceptance by us and the Participating Restaurant. When you place your order online, we will send you an email to confirm that we have received it. This email confirmation will be produced automatically so that you have confirmation of your order details. You must inform us immediately if any details are incorrect. The fact that you receive an automatic confirmation does not necessarily mean that either we or the Participating Restaurant will be able to fill your order. Once we have sent the confirmation email we will check availability and delivery capacity.</li>
                        <li>If the ordered Food Delivery and delivery capacity is available, the Participating Restaurant will accept the contract and confirm it to farsa. If the details of the order are correct, the contract for the Food Delivery, Goods or Services will be confirmed by text message (SMS).</li>
                        <li>In the case that Goods offered by farsa were ordered, farsa will confirm availability together with or separately from Food Delivery.</li>
                        <li>The confirmation message will specify delivery details including the approximate delivery time specified by the Participating Restaurant and confirm the price of the Food Delivery, Goods and Services ordered.</li>
                        <li>If the Food Delivery and/or Goods are not available or if there is no delivery capacity, we will also let you know by text message (SMS) or phone call.</li>
                    </ul>
                    

                </div>
            </div>
        </article>

            <!-- Section -->
            <section class="section section-lg dark bg-dark">
                <!-- BG Image -->
                <div class="bg-image bg-parallax"><img src="frontend/assets/img/bg-croissant.jpg" alt=""></div>
                <div class="container text-center">
                    <div class="col-lg-8 push-lg-2">
                        <h2 class="mb-3">Would you like to visit Us?</h2>
                        <h5 class="text-muted">Book a table even right now or make an online order!</h5>
                        <a href="menu" class="btn btn-primary"><span>Order Online</span></a>
                        <a href="bookyourtable" class="btn btn-outline-primary"><span>Book a table</span></a>
                    </div>
                </div>
            </section>
@endsection

