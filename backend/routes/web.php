<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='Admin')
            return view('app.index');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }

});

//Page Routes......................
Route::get('/about-us', function () {
    return view('frontend.about');
});

Route::get('/tandc', function () {
    return view('frontend.tandc');
});

Route::get('/facilities', function () {
    return view('frontend.facilities');
});

Route::get('/reviews', function () {
    return view('frontend.reviews');
});

Route::get('/book-table', function () {
    return view('frontend.booktable');
});

Route::get('/menu', function () {
    return view('frontend.menu');
});

Route::get('/offers', function () {
    return view('frontend.offer');
});

Route::get('/contact', function () {
    return view('frontend.contact');
});

Route::get('/web/getMyProduct/{id}', function ($id) {
    return \App\Product::where('id',$id)->with('prices')->get()->first();
});

Route::get('/account', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='User')
            return view('frontend.account');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});

Route::get('/myorders', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='User')
            return view('frontend.myorder');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});

Route::get('/checkout', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='User')
            return view('frontend.checkout');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});

Route::get('/payment_failed', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='User')
            return view('frontend.payment_failed');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});

Route::get('/payment_success', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='User')
            return view('frontend.payment_success');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});

Route::get('/web/takeorder', function () {
    return view('frontend.menu');
});

Route::get('images/{filename}', function ($filename)
{
    $file = \Illuminate\Support\Facades\Storage::get($filename);
    return response($file, 200)->header('Content-Type', 'image/jpeg');
    //return base64_encode($file);
});

Route::post('/web/booking-form', 'Auth\BookTableController@bookMyTable');
Route::post('/web/submitreview', 'Auth\TestimonialController@submitMyReview')->name('submitMyReview');
Route::post('/web/submitmydeliverydddress', 'Auth\AddressController@submitMyDeliveryAddress')->name('submitMyDeliveryAddress');
Route::post('/web/changemypassword', 'Auth\UserController@changeMyPassword')->name('changeMyPassword');
Route::post('/web/takeorder', 'Auth\OrderController@takeMyOrder')->name('takeMyOrder');
Route::post('/web/removemyorder', 'Auth\OrderController@removeMyOrder')->name('removeMyOrder');
Route::post('/web/makemypayment', 'Auth\OrderController@makeMyPayment')->name('makeMyPayment');


Route::post('indipay/success_response', 'Auth\OrderController@successResponse');
Route::post('indipay/failure_response', 'Auth\OrderController@failureResponse');


Route::get('/home', 'HomeController@index')->name('home');

//TODO add middleware to authenticate
//Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
Route::group(['prefix' => 'api'], function () {
    //Rest resources
    Route::resource('user', 'Auth\UserController');
    Route::resource('testimonial', 'Auth\TestimonialController');
    Route::resource('menu', 'Auth\MenuController');
    Route::resource('product', 'Auth\ProductController');
    Route::resource('weekendoffer', 'Auth\WeekendOfferController');
    Route::resource('slider', 'Auth\SliderController');
    Route::resource('offer', 'Auth\OfferController');
    Route::resource('order', 'Auth\OrderController');
    Route::resource('bookedtables', 'Auth\BookTableController');
    Route::resource('address', 'Auth\AddressController');
    Route::resource('pincode', 'Auth\PincodeController');

    Route::post('changeUserStatus', 'Auth\UserController@changeUserStatus');
    Route::post('changeTestimonialStatus', 'Auth\TestimonialController@changeTestimonialStatus');
    Route::post('changeMenuStatus', 'Auth\MenuController@changeMenuStatus');
    Route::post('changeProductStatus', 'Auth\ProductController@changeProductStatus');
    Route::post('changeWeekendOfferStatus', 'Auth\WeekendOfferController@changeWeekendOfferStatus');
    Route::post('changeBookTableStatus', 'Auth\BookTableController@changeBookTableStatus');
    Route::post('changeOrderStatus', 'Auth\OrderController@changeOrderStatus');
    Route::post('changePincodeStatus', 'Auth\PincodeController@changePincodeStatus');


    Route::post('getProductPriceList', 'Auth\ProductController@getProductPriceList');
    Route::post('getOrderPdf', 'Auth\OrderController@getOrderPdf');
    Route::post('updateMyProfile', 'Auth\UserController@updateMyProfile');
    Route::post('changePassword', 'Auth\UserController@changeAdminPassword');

    Route::get('getDashboardInfo', 'HomeController@getDashboardInfo');

    Route::get('getMyProfile', 'HomeController@getMyProfile');
});


//Load angular templates
//TODO user permission validation
Route::get('template/{name}', ['as' => 'templates', function ($name) {
    return view('app.'.$name);
}]);
Auth::routes();

Route::get('/test', function () {
   /* $pdf = \Illuminate\Support\Facades\App::make('dompdf.wrapper');

    $orders=\App\Order::with('orderitems.product.product')->with('address')->get();

    $data='<!DOCTYPE html><html><head><style>table, td, th,tr {border: 0.01em solid #858b8c;} td{padding-left: 10px;padding-right: 5px;padding-bottom: 5px;padding-top: 5px;}</style></head><body>';
    $data=$data.'<h2 style="text-align: center;">Order Details</h2>';
    $data=$data.'<table  style="width:100%;"><tr><th>SlNo</th><th>Date</th><th>Name & Address</th><th>Orders</th><th>Payment Type</th><th>Amount</th></tr>';

    foreach($orders as $index=> $order) {
        $data = $data . '<tr><td>'.($index+1).'</td><td>'.\Illuminate\Support\Carbon::parse($order->datetime)->format('d/m/y').'</td>';

        $data=$data.'<td>'.$order->address->name.'<br/>'.$order->address->phone.','.$order->address->phone2.'<br/>'.$order->address->street.'<br/>'.$order->address->landmark.'<br/>'.$order->address->address.'</td>';

        $data = $data . '<td>';
        $data = $data . '<table><tr><th>Item</th><th>Qty</th><th>E.Price</th></tr>';
        foreach($order->orderitems as $item) {
            if($item->product!=null)
            $data = $data . '<tr><td>'.$item->product->product->name.'<br/>'.$item->product->name.'</td><td>'.$item->qty.'</td><td>'.$item->product->amount.'</td></tr>';
        }
        $data = $data . '<tr style="color:blue;"><td colspan="2">GST</td><td>'.$order->gst.'</td></tr>';
        $data = $data . '<tr style="color:blue;"><td colspan="2">Delivery</td><td>'.$order->deliverycharge.'</td></tr>';
        $data = $data . '</table>';
        $data = $data . '</td>';

        $color='';
        if($order->paymenttype=="COD")
            $color='red';
        $data=$data.'<td style="color:'.$color.'">'.$order->paymenttype.'</td><td style="color:blue;">'.$order->grandtotal.'</td></tr>';
    }
    $data=$data.'</table></body></html>';



    $pdf->loadHTML($data);
    $pdf->setPaper('a4', 'landscape');
    return $pdf->stream();*/

   /* $lists=[];
    $list['product']=['Item 1','Item 2','Item 3'];
    array_push($lists,$list);
   // return $lists[0]['product'];

    if(array_key_exists('productd',$lists[0])){
        return 'True';
    }else{
        return 'False';
    }*/

    //return storage_path();

    return \App\Helper\SMSHelper::sendSMS('9633720788','Hai shahid test');
});


Route::get('{any}', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='Admin')
            return view('app.index');
        else
            return view('frontend.index');
    }
    else{
        return view('frontend.index');
    }
});