let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
    .copyDirectory('resources/assets/img', 'public/img')
    .copyDirectory('resources/assets/font-awesome', 'public/font-awesome')
    .copyDirectory('resources/assets/frontend', 'public/frontend')
    .copyDirectory('resources/assets/img', 'public/img')
    .combine(['resources/assets/js/angular/angular.min.js',
        'resources/assets/js/angular/angular-resource.min.js',
        'resources/assets/js/angular/angular-route.min.js',
        'resources/assets/js/angular/ui-bootstrap-tpls.min.js',
        'resources/assets/js/angular/angular-notify.js',
        'resources/assets/js/angular/textAngular-rangy.min.js',
        'resources/assets/js/angular/textAngular-sanitize.min.js',
        'resources/assets/js/angular/textAngular.min.js',
        'resources/assets/js/angular/angular-confirm.min.js',
        'resources/assets/js/angular/angularscript.js',
        'resources/assets/js/angular/controller/*',
        'resources/assets/js/angular/service/*'
    ],'public/js/app.js');

